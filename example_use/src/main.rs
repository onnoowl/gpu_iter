#![feature(test)]
#![allow(clippy::blacklisted_name)]

mod ocl_demo;

#[cfg(test)]
mod ocl_bench;

use gpu_iter_derive::ToGpu;

#[derive(Debug, Default, PartialEq, Copy, Clone, ToGpu)]
#[repr(C)]
struct UnnamedFoo(u16, f32);

#[derive(Debug, Default, PartialEq, Copy, Clone, ToGpu)]
#[repr(C)]
struct Foo {
    x: u16,
    y: f32,
}

#[derive(Debug, Default, PartialEq, Copy, Clone, ToGpu)]
#[repr(C)]
struct FooBar {
    foo: Foo,
    bar: f32,
}

fn main() {}

#[cfg(test)]
mod tests {
    use super::*;
    use gpu_iter::gpu_iter::IntoGpuIter;
    use gpu_iter::graph::IntoExpr;
    use gpu_iter::to_gpu::HasGPUType;

    #[test]
    fn foo_render() {
        let var = 1u16.into_expr();
        let g = Foo::from_expr(var + 2, 2.0);
        assert_eq!(
            format!("{}", g),
            "Foo __gpu_iter_result = (Foo) {((ushort)1 + (ushort)2), (float)2};\n",
        );

        assert_eq!(
            format!("{}", g.x()),
            "ushort __gpu_iter_result = ((Foo) {((ushort)1 + (ushort)2), (float)2}).x;\n",
        );

        assert_eq!(
            format!("{}", Foo::render_struct_defn()),
            "typedef struct tag_Foo\n{\n\tushort\tx;\n\tfloat\ty;\n} Foo;\n",
        );

        assert_eq!(
            format!("{}", UnnamedFoo::render_struct_defn()),
            "typedef struct tag_UnnamedFoo\n{\n\tushort\ts0;\n\tfloat\ts1;\n} UnnamedFoo;\n",
        );
    }

    #[test]
    fn compose_run() {
        let data: Vec<u16> = vec![1, 2];
        let summed = data
            .gpu_iter()
            .map(|i| (i + 2) * 3)
            .map(|i| Foo::from_expr(i, 2.0))
            .collect()
            .unwrap();

        assert_eq!(summed, [Foo { x: 9, y: 2.0 }, Foo { x: 12, y: 2.0 }]);
    }

    #[test]
    fn program_run() {
        let data = vec![Foo { x: 2, y: 3.0 }, Foo { x: 4, y: 5.0 }];
        let summed = data
            .gpu_iter()
            .map(|foo| foo.x().cast::<f32>() + foo.y())
            .collect()
            .unwrap();
        assert_eq!(summed, [5.0, 9.0]);
    }

    // TODO: Investigate failure
    #[ignore]
    #[test]
    fn foobar() {
        let data = vec![
            FooBar {
                foo: Foo { x: 2, y: 3.0 },
                bar: 4.0,
            },
            FooBar {
                foo: Foo { x: 4, y: 5.0 },
                bar: 4.0,
            },
        ];
        let summed = data
            .gpu_iter()
            .map(|foobar| foobar.foo().x().cast::<f32>() + foobar.foo().y())
            .collect()
            .unwrap();
        assert_eq!(summed, [5.0, 9.0]);
    }

    // TODO: Investigate failure
    #[ignore]
    #[test]
    fn foobar_compose() {
        let data = vec![1, 2];
        let summed = data
            .gpu_iter()
            .map(|i| FooBar::from_expr(Foo::from_expr(i, 2.0), 3.0))
            .collect()
            .unwrap();

        assert_eq!(
            summed,
            [
                FooBar {
                    foo: Foo { x: 1, y: 2.0 },
                    bar: 3.0
                },
                FooBar {
                    foo: Foo { x: 2, y: 2.0 },
                    bar: 3.0
                }
            ]
        );
    }

    // TODO: Investigate failure
    #[ignore]
    #[test]
    fn f64() {
        let data = vec![Foo { x: 2, y: 3.0 }, Foo { x: 4, y: 5.0 }];
        let summed = data
            .gpu_iter()
            .map(|foo| foo.x().cast::<f64>() + foo.y().cast::<f64>())
            .collect()
            .unwrap();
        assert_eq!(summed, [5.0, 9.0]);
    }
}
