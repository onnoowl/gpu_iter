#[cfg(test)]
mod tests {
    extern crate test;
    use gpu_iter::ocl_setup::ContextQueue;
    use ocl::{Buffer, Kernel, Program};
    use test::Bencher;

    #[bench]
    fn program_building(b: &mut Bencher) -> ocl::Result<()> {
        let src = r#"
            __kernel void add(__global float* buffer, float scalar) {
                bool a = 2;
                buffer[get_global_id(0)] += scalar + a;
            }
        "#;

        let context_queue = ContextQueue::with_best_device().unwrap();

        let mut program_builder = Program::builder();
        program_builder.src(src);

        b.iter(|| program_builder.build(&context_queue.context).unwrap());

        Ok(())
    }

    #[bench]
    fn kernel_building(b: &mut Bencher) -> ocl::Result<()> {
        let src = r#"
            __kernel void add(__global float* buffer, float scalar) {
                bool a = 2;
                buffer[get_global_id(0)] += scalar + a;
            }
        "#;

        let len = 1 << 20;

        let context_queue = ContextQueue::with_best_device().unwrap();

        let program = Program::builder().src(src).build(&context_queue.context)?;

        let buffer = Buffer::<f32>::builder()
            .len(len)
            .context(&context_queue.context)
            .build()?;

        let mut kernel_builder = Kernel::builder();
        kernel_builder
            .program(&program)
            .name("add")
            .queue(context_queue.queue.clone())
            .global_work_size(len)
            .arg(&buffer)
            .arg(10.0f32);

        b.iter(|| kernel_builder.build().unwrap());

        Ok(())
    }
}
