#[cfg(test)]
mod tests {
    use ocl::ProQue;

    fn dummy<F: Fn() -> u32>(f: F) -> u32 {
        f()
    }

    #[test]
    fn lambdas_unique() {
        use std::any::Any;

        for i in [1, 2, 3].iter() {
            let f = move || i + 2u32;
            println!("{} : {} : {:?}", i, dummy(f), Any::type_id(&f));
        }
    }

    #[test]
    fn trivial() -> ocl::Result<()> {
        let src = r#"
            __kernel void add(__global float* buffer, float scalar) {
                bool a = 2;
                int3 foobar = (int3)(1, 2, 3);
                buffer[get_global_id(0)] += scalar + a;
            }
        "#;

        let pro_que = ProQue::builder().src(src).dims(1 << 20).build()?;

        let buffer = pro_que.create_buffer::<f32>()?;

        let kernel = pro_que
            .kernel_builder("add")
            .arg(&buffer)
            .arg(10.0f32)
            .build()?;

        unsafe {
            kernel.enq()?;
        }

        let i = 200_007;
        let mut vec = vec![0.0f32; buffer.len()];
        buffer.read(&mut vec).enq()?;

        println!("The value at index [{}] is now '{}'!", i, vec[i]);
        Ok(())
    }

    #[test]
    fn longer() -> ocl::Result<()> {
        let src = r#"
            __kernel void add(__global float* buffer, float scalar) {
                int a = 3;
                {
                    bool a = 2;
                    buffer[get_global_id(0)] += scalar + a;
                }
                buffer[get_global_id(0)] = a;
            }
        "#;

        let pro_que = ProQue::builder().src(src).dims(1 << 20).build()?;

        let buffer = pro_que.create_buffer::<f32>()?;

        let kernel = pro_que
            .kernel_builder("add")
            .arg(&buffer)
            .arg(10.0f32)
            .build()?;

        unsafe {
            kernel.enq()?;
        }

        let i = 200_007;
        let mut vec = vec![0.0f32; buffer.len()];
        buffer.read(&mut vec).enq()?;

        println!("The value at index [{}] is now '{}'!", i, vec[i]);
        Ok(())
    }
}
