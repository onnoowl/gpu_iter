mod expr;
mod expr_types;
mod node;
mod render;
mod resolves_to_type;

pub use expr::*;
pub use expr_types::*;
pub use node::*;
pub use render::*;
pub use resolves_to_type::*;

use crate::get_type_str::{DynRenderTypeStr, RenderTypeStr};

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn ocl_const_from() {
        let _: Literal<_> = 3i32.into();
    }

    #[test]
    fn ocl_basic_render() {
        let a: Expr<f32> = 3.0.into();
        let b = (&a + &a) * 2.0;
        let c = (&b * &b).cast::<f64>() + 3.0;
        let out = c.max(9.0);
        // println!("{:?}", v.max(9));
        // println!("{}", out.render());
        assert_eq!(
            format!("{}", out),
            "float __gpu_iter_var0 = (float)3;\n\
            float __gpu_iter_var1 = ((__gpu_iter_var0 + __gpu_iter_var0) * (float)2);\n\
            double __gpu_iter_result = max(((double)(__gpu_iter_var1 * __gpu_iter_var1) + (double)3), (double)9);\n",
        );
    }

    // #[test]
    // fn ocl_basic_render() {
    //     let a: Expr<_> = 3.into();
    //     let b = (&a + &a) * 2u64;
    //     let c = &b * &b + 3.0;
    //     let out = c.max(9);
    //     // println!("{:?}", v.max(9));
    //     // println!("{}", out.render());
    //     assert_eq!(
    //         format!("{}", out),
    //         "int __gpu_iter_var0 = (int)3;\n\
    //         ulong __gpu_iter_var1 = ((__gpu_iter_var0 + __gpu_iter_var0) * (ulong)2);\n\
    //         double __gpu_iter_result = max(((__gpu_iter_var1 * __gpu_iter_var1) + (double)3), (int)9);\n",
    //     );
    // }

    #[test]
    fn ocl_if_render() {
        /*
        int __gpu_iter_var0;
        if(((int)5 == (int)2)) {
            __gpu_iter_var0 = (int)42;
        } else {
            __gpu_iter_var0 = (int)3;
        }
        int __gpu_iter_result = __gpu_iter_var0;
        */

        let a: Expr<_> = 5.into();
        let out = (a.eq(2)).then(42).els(3);

        // println!("{}", out.render());
        assert_eq!(
            format!("{}", out),
            "int __gpu_iter_var0;\n\
             if(((int)5 == (int)2)) {\n    \
             __gpu_iter_var0 = (int)42;\n\
             } else {\n    \
             __gpu_iter_var0 = (int)3;\n\
             }\n\
             int __gpu_iter_result = __gpu_iter_var0;\n",
        )
    }

    #[test]
    fn ocl_multi_if_render() {
        /*
        int __gpu_iter_var1;
        if(((int)5 == (int)2)) {
            __gpu_iter_var1 = (int)42;
        } else {
            __gpu_iter_var1 = (int)3;
        }
        int __gpu_iter_var2 = (__gpu_iter_var1 + (int)2);
        int __gpu_iter_var0;
        if(((__gpu_iter_var2 + (int)3) == (int)45)) {
            __gpu_iter_var0 = (int)8;
        } else {
            __gpu_iter_var0 = __gpu_iter_var2;
        }
        int __gpu_iter_result = __gpu_iter_var0;
        */

        let a: Expr<_> = 5.into();
        let b = (a.eq(2)).then(42).els(3) + 2;
        let out = ((&b + 3).eq(45)).then(8).els(&b);

        assert_eq!(
            format!("{}", out),
            "int __gpu_iter_var1;\n\
             if(((int)5 == (int)2)) {\n    \
             __gpu_iter_var1 = (int)42;\n\
             } else {\n    \
             __gpu_iter_var1 = (int)3;\n\
             }\n\
             int __gpu_iter_var2 = (__gpu_iter_var1 + (int)2);\n\
             int __gpu_iter_var0;\n\
             if(((__gpu_iter_var2 + (int)3) == (int)45)) {\n    \
             __gpu_iter_var0 = (int)8;\n\
             } else {\n    \
             __gpu_iter_var0 = __gpu_iter_var2;\n\
             }\n\
             int __gpu_iter_result = __gpu_iter_var0;\n",
        )
    }
}
