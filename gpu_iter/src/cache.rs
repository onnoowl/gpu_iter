use lazy_static::lazy_static;
use linked_hash_map::LinkedHashMap;

use std::sync::RwLock;

use ocl::Program;

use crate::error::Error;
use crate::ocl_setup::ContextQueue;

// TODO: way to control cache size
static DEFAULT_CACHE_SIZE: usize = 32;

lazy_static! {
    pub(crate) static ref COMPILE_CACHE: RwLock<LinkedHashMap<u64, CachedProgram>> =
         RwLock::new(LinkedHashMap::with_capacity(DEFAULT_CACHE_SIZE)) ;
}

#[derive(Clone, Debug)]
pub(crate) struct CachedProgram {
    pub program: Program,
    pub program_name: String,
    pub context_queue: ContextQueue,
    pub output_buf_index: Option<usize>,
}

// Defaults to LRI mode
pub(crate) fn get_or_cache<F: FnOnce() -> Result<CachedProgram, Error>>(
    program_id: u64,
    f: F,
) -> Result<CachedProgram, Error> {
    lri_get_or_cache(program_id, f)
}

// Attempts to use a read lock, and if the cached value is not present, switches to write lock in order to insert.
// If the cache is full, deletes the least recently inserted (lri) entry.
pub(crate) fn lri_get_or_cache<F: FnOnce() -> Result<CachedProgram, Error>>(
    program_id: u64,
    f: F,
) -> Result<CachedProgram, Error> {
    let map = COMPILE_CACHE.read().unwrap();

    // This should be a get_refresh, but then we would need a write lock each time.
    // Now we have a LRI (least recently inserted) cache instead of a LRU (least recently used) cache.
    // TODO: Is this worth it? Probably only have 1 thread using GPU...
    let cached = map.get(&program_id);

    if let Some(inner) = cached {
        Ok(inner.clone())
    } else {
        // Make sure we drop the read lock acquisition
        drop(map);

        // To insert, we need a write lock, which is the same as just switching to LRU mode.
        // lru_get_or_cache uses get_refresh. Since we're only using it if we can't find the value,
        // That makes us essentially an lri cache instead of an lru cache.
        lru_get_or_cache(program_id, f)
    }
}

// Uses a write lock, even if cache value already exists.
// If the cache is full, deletes the least recently used (lru) entry.
pub(crate) fn lru_get_or_cache<F: FnOnce() -> Result<CachedProgram, Error>>(
    program_id: u64,
    f: F,
) -> Result<CachedProgram, Error> {
    let mut map = COMPILE_CACHE.write().unwrap();

    if let Some(inner) = map.get_refresh(&program_id) {
        Ok(inner.clone())
    } else {
        let new_value = f()?;
        map.insert(program_id, new_value.clone());

        if map.len() == map.capacity() {
            map.pop_front();
        }

        Ok(new_value)
    }
}
