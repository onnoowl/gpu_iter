mod iter_props;
mod layers;
mod src_type;

use crate::cache::{self, CachedProgram};
// use crate::graph::{ExprEnum, ExprInner};
use crate::error::Error;
use crate::get_type_str::{DynRenderTypeStr, RenderTypeStr};
use crate::graph::{DynNode, Expr, InputVar, NodeMetaData, RawMap, VarName};
pub use iter_props::*;
pub use layers::*;
pub use src_type::*;

use impl_trait_for_tuples::impl_for_tuples;
use ocl::{traits::OclPrm, Buffer, Kernel, Program};
use smallvec::{smallvec, SmallVec};
use tuple_utils::Merge;

use std::any::{Any, TypeId};
use std::collections::hash_map::{DefaultHasher, HashMap};
use std::fmt;
use std::hash::{Hash, Hasher};
use std::marker::PhantomData;

pub trait ExprOrTuple: Clone + fmt::Debug {
    type DynNodes: fmt::Debug + Clone;
    type VarNames;

    fn get_var_names(
        dyn_nodes: Self::DynNodes,
        meta_map: &HashMap<u64, NodeMetaData>,
    ) -> Self::VarNames;
    fn call_on_dyn_nodes(dyn_nodes: Self::DynNodes, fun: &mut dyn FnMut(&[DynNode]));
    fn to_dyn_nodes(&self) -> Self::DynNodes;
}

#[allow(clippy::type_complexity)]
#[impl_for_tuples(1, 12)]
impl ExprOrTuple for TupleIdentifier {
    for_tuples!( type DynNodes = ( #( TupleIdentifier::DynNodes),* ); );
    for_tuples!( type VarNames = ( #( TupleIdentifier::VarNames ),* ); );

    fn get_var_names(
        dyn_nodes: Self::DynNodes,
        meta_map: &HashMap<u64, NodeMetaData>,
    ) -> Self::VarNames {
        for_tuples!( ( #( TupleIdentifier::get_var_names(dyn_nodes.TupleIdentifier, meta_map) ),* ) )
    }
    fn call_on_dyn_nodes(dyn_nodes: Self::DynNodes, fun: &mut dyn FnMut(&[DynNode])) {
        for_tuples!( #( TupleIdentifier::call_on_dyn_nodes(dyn_nodes.TupleIdentifier, fun); )* )
    }
    fn to_dyn_nodes(&self) -> Self::DynNodes {
        for_tuples!( ( #( TupleIdentifier::to_dyn_nodes(&self.TupleIdentifier) ),* ) )
    }
}

// ExprOrTuple is a trait for Expr, or for tuples of Expr, i.e. (Expr, ...)
impl<T: RenderTypeStr> ExprOrTuple for Expr<T> {
    type VarNames = VarName;
    type DynNodes = DynNode;

    fn get_var_names(
        dyn_node: Self::DynNodes,
        meta_map: &HashMap<u64, NodeMetaData>,
    ) -> Self::VarNames {
        let var_num = meta_map
            .get(&dyn_node.default_ptr_hash())
            .expect(
                "Expr used by RawMap failed to get a meta entry created during graph traversal.",
            )
            .var_num
            .expect("Expr used by RawMap failed to have var_num assigned by graph traversal.");
        VarName { var_num }
    }
    fn call_on_dyn_nodes(dyn_node: Self::DynNodes, fun: &mut dyn FnMut(&[DynNode])) {
        fun(&[dyn_node])
    }
    fn to_dyn_nodes(&self) -> Self::DynNodes {
        self.node.clone()
    }
}

// ExprTuple is only a trait for real tuples of Expr, i.e. (Expr, ...). Not just an Expr
// There is no specific reason for `InputVar<u8>`, we just need to make sure it merges with
// something that implements Node.
pub trait ExprTuple: ExprOrTuple + Merge<()> {}
impl<T: ExprOrTuple + Merge<()>> ExprTuple for T {}

pub struct GpuIter<'s: 'd, 'd, E: ExprOrTuple> {
    props: DiscoveredProperties,
    sources: SmallVec<[Src<'s, 'd>; 8]>,
    min_len: usize,
    layer_hashes: DefaultHasher,
    result_expr: E,
}

impl<'s: 'd, 'd, E: ExprOrTuple + 'static> GpuIter<'s, 'd, E> {
    pub fn map<EOUT: ExprOrTuple + Any, F: FnOnce(E) -> EOUT + Any>(
        // pub fn map<EOUT: ExprOrTuple + Any, F: FnOnce<E, Output = EOUT>>(
        self,
        f: F,
    ) -> GpuIter<'s, 'd, EOUT> {
        let mut layer_hashes = self.layer_hashes;
        TypeId::of::<F>().hash(&mut layer_hashes);

        let result_expr = f(self.result_expr.clone());
        // let result_expr = f.call_once(self.result_expr.clone());
        GpuIter {
            props: self.props,
            sources: self.sources,
            min_len: self.min_len,
            layer_hashes,
            result_expr,
        }
    }

    pub fn raw_map<OutT: RenderTypeStr + Any, F>(self, f: F) -> GpuIter<'s, 'd, Expr<OutT>>
    where
        F: Fn(&mut fmt::Formatter<'_>, VarName, E::VarNames) -> fmt::Result + 'static,
    {
        let mut layer_hashes = self.layer_hashes;
        TypeId::of::<F>().hash(&mut layer_hashes);
        TypeId::of::<OutT>().hash(&mut layer_hashes);

        let result_expr = Expr::new(RawMap::new(self.result_expr, f));
        GpuIter {
            props: self.props,
            sources: self.sources,
            min_len: self.min_len,
            layer_hashes,
            result_expr,
        }
    }

    pub fn raw_map_carry<OutT: RenderTypeStr + Any, F>(
        self,
        f: F,
    ) -> GpuIter<'s, 'd, (Expr<OutT>, E)>
    where
        F: Fn(&mut fmt::Formatter<'_>, VarName, E::VarNames) -> fmt::Result + 'static,
    {
        let mut layer_hashes = self.layer_hashes;
        TypeId::of::<F>().hash(&mut layer_hashes);
        TypeId::of::<OutT>().hash(&mut layer_hashes);

        let result_raw_expr = Expr::new(RawMap::new(self.result_expr.clone(), f));
        GpuIter {
            props: self.props,
            sources: self.sources,
            min_len: self.min_len,
            layer_hashes,
            result_expr: (result_raw_expr, self.result_expr),
        }
    }
}

pub trait Zip<'s: 'd, 'd, E, E2: ExprOrTuple, EOUT: ExprOrTuple> {
    fn zip(self, other: GpuIter<'s, 'd, E2>) -> GpuIter<'s, 'd, EOUT>;
}

fn raw_zip<'s: 'd, 'd, E: ExprOrTuple, E2: ExprOrTuple, EOUT: ExprTuple>(
    a: GpuIter<'s, 'd, E>,
    b: GpuIter<'s, 'd, E2>,
    result_expr: EOUT,
) -> GpuIter<'s, 'd, EOUT> {
    let mut sources = a.sources;
    sources.extend(b.sources.into_iter());
    let mut hasher = a.layer_hashes;
    101u64.hash(&mut hasher); //unique prime to mark a zip op
    b.layer_hashes.finish().hash(&mut hasher);

    GpuIter {
        props: a.props.merge(b.props),
        sources,
        min_len: a.min_len.min(b.min_len),
        layer_hashes: hasher,
        result_expr,
    }
}

//Zip for gpu_iters with single Expr & Expr
impl<'s: 'd, 'd, T1: RenderTypeStr, T2: RenderTypeStr>
    Zip<'s, 'd, Expr<T1>, Expr<T2>, (Expr<T1>, Expr<T2>)> for GpuIter<'s, 'd, Expr<T1>>
{
    fn zip(self, other: GpuIter<'s, 'd, Expr<T2>>) -> GpuIter<'s, 'd, (Expr<T1>, Expr<T2>)> {
        let result_expr = (self.result_expr.clone(), other.result_expr.clone());
        raw_zip(self, other, result_expr)
    }
}

//Zip for gpu_iters with (Expr,...) & (Expr,...)
impl<'s: 'd, 'd, E: ExprTuple, E2: ExprTuple> Zip<'s, 'd, E, E2, <E as Merge<E2>>::Output>
    for GpuIter<'s, 'd, E>
where
    E: Merge<E2>,
    <E as Merge<E2>>::Output: ExprTuple,
{
    fn zip(self, other: GpuIter<'s, 'd, E2>) -> GpuIter<'s, 'd, <E as Merge<E2>>::Output> {
        let result_expr = self.result_expr.clone().merge(other.result_expr.clone());
        raw_zip(self, other, result_expr)
    }
}

//Zip for gpu_iters with Expr & (Expr,...)
impl<'s: 'd, 'd, T: RenderTypeStr, E2: ExprTuple>
    Zip<'s, 'd, Expr<T>, E2, <(Expr<T>,) as Merge<E2>>::Output> for GpuIter<'s, 'd, Expr<T>>
where
    (Expr<T>,): Merge<E2>,
    <(Expr<T>,) as Merge<E2>>::Output: ExprTuple,
{
    fn zip(self, other: GpuIter<'s, 'd, E2>) -> GpuIter<'s, 'd, <(Expr<T>,) as Merge<E2>>::Output> {
        let result_expr = (self.result_expr.clone(),).merge(other.result_expr.clone());
        raw_zip(self, other, result_expr)
    }
}

//Zip for gpu_iters with (Expr,...) & Expr
impl<'s: 'd, 'd, T: RenderTypeStr, E: ExprTuple>
    Zip<'s, 'd, E, Expr<T>, <E as Merge<(Expr<T>,)>>::Output> for GpuIter<'s, 'd, E>
where
    E: Merge<(Expr<T>,)>,
    <E as Merge<(Expr<T>,)>>::Output: ExprTuple,
{
    fn zip(
        self,
        other: GpuIter<'s, 'd, Expr<T>>,
    ) -> GpuIter<'s, 'd, <E as Merge<(Expr<T>,)>>::Output> {
        let result_expr = self.result_expr.clone().merge((other.result_expr.clone(),));
        raw_zip(self, other, result_expr)
    }
}

pub trait IntoGpuIter<'s: 'd, 'd, T: RenderTypeStr> {
    fn gpu_iter(self) -> GpuIter<'s, 'd, Expr<T>>;
}

pub trait IntoGpuIterMut<'s: 'd, 'd, T: RenderTypeStr> {
    fn gpu_iter_mut(self) -> GpuIter<'s, 'd, Expr<T>>;
}

impl<'s: 'd, 'd, T, I> IntoGpuIter<'s, 'd, T> for I
where
    T: RenderTypeStr + OclPrm + Any,
    I: ImmutSliceToSrcType<'s, 'd, SliceRefType = T>,
{
    fn gpu_iter(self) -> GpuIter<'s, 'd, Expr<T>> {
        let len = self.len();
        let s = Src::new(self.to_src_type());
        let num = s.var_num;
        GpuIter {
            props: Default::default(),
            sources: smallvec!(s),
            min_len: len,
            layer_hashes: Default::default(),
            result_expr: Expr::new(InputVar::new(num)),
        }
    }
}

impl<'s: 'd, 'd, T, I> IntoGpuIterMut<'s, 'd, InputVar<T>> for I
where
    T: RenderTypeStr + OclPrm + Any,
    I: MutSliceToSrcType<'s, 'd, SliceRefType = T>,
{
    fn gpu_iter_mut(self) -> GpuIter<'s, 'd, Expr<InputVar<T>>> {
        let len = self.len();
        let s = Src::new(self.to_src_type());
        let num = s.var_num;
        GpuIter {
            props: Default::default(),
            sources: smallvec!(s),
            min_len: len,
            layer_hashes: Default::default(),
            result_expr: Expr::new(InputVar::new(num)),
        }
    }
}

struct ArgFormatter<'s: 'd, 'd, 'c: 'd, T: RenderTypeStr>(&'c [Src<'s, 'd>], bool, PhantomData<T>);

impl<'s: 'd, 'd, 'c: 'd, T: RenderTypeStr> fmt::Display for ArgFormatter<'s, 'd, 'c, T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let sources = self.0;
        let needs_output_buf = self.1;

        for (i, s) in sources.iter().enumerate() {
            match &s.src_type {
                SrcType::SAB(_) => {
                    write!(f, "__global {type} buffer{var_num}", type=s.src_type.render_type(), var_num=s.var_num)?
                }
                _ => unimplemented!(),
                // SrcType::ScalarConst(val) => {
                //     write!(f, "{type} var{var_num}", type=val.get_scalar_type(), var_num=s.var_num)?
                // }
            }
            if i < sources.len() - 1 {
                write!(f, ", ")?;
            }
        }
        if needs_output_buf {
            write!(f, ", __global {type}* buffer_output", type=<T>::render_type())?;
        }
        Ok(())
    }
}

struct ArgSetupFormatter<'s: 'd, 'd, 'c: 'd>(&'c [Src<'s, 'd>]);

impl<'s: 'd, 'd, 'c: 'd> fmt::Display for ArgSetupFormatter<'s, 'd, 'c> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let sources = self.0;

        for s in sources {
            match &s.src_type {
                SrcType::SAB(ref sab) => {
                    sab.deref_impl_render(f)?; // Render the type for the buffer de-refed
                    writeln!(
                        f,
                        " var{var_num} = buffer{var_num}[get_global_id(0)];",
                        var_num = s.var_num
                    )?
                }
                _ => unimplemented!(),
                // SrcType::ScalarConst(_) => (),
            }
        }

        Ok(())
    }
}

struct OutCleanupFormatter<'s: 'd, 'd, 'c: 'd, T: RenderTypeStr>(
    &'c [Src<'s, 'd>],
    Option<usize>,
    PhantomData<T>,
);

impl<'s: 'd, 'd, 'c: 'd, T: RenderTypeStr> fmt::Display for OutCleanupFormatter<'s, 'd, 'c, T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let sources = self.0;
        let output_buf_index = self.1;

        if let Some(i) = output_buf_index {
            writeln!(
                f,
                "buffer{var_num}[get_global_id(0)] = ({type})__gpu_iter_result;",
                type=<T>::render_type(),
                var_num = sources[i].var_num
            )
        } else {
            writeln!(
                f,
                "buffer_output[get_global_id(0)] = ({type})__gpu_iter_result;",
                type=<T>::render_type()
            )
        }
    }
}

struct StructDefnWriter<T: RenderTypeStr>(Expr<T>);

impl<T: RenderTypeStr> fmt::Display for StructDefnWriter<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.write_struct_defns(f)
    }
}

// __gpu_iter_result
impl<'s: 'd, 'd, T: RenderTypeStr + OclPrm> GpuIter<'s, 'd, Expr<T>> {
    pub fn collect(mut self) -> Result<Vec<T>, Error> {
        let program_id = self.layer_hashes.finish();

        // TODO: Resolve caching bug if lambda uses outside data that changes
        // under those circumstances, the hash of the lambda sig is the same, but the behavior differs.
        let cached_program = cache::get_or_cache(program_id, || {
            let struct_defn = StructDefnWriter(self.result_expr.clone());
            let program_name = format!("program{}", program_id);
            let output_buf_index = self.sources.iter().enumerate().find_map(|(i, s)| {
                if let SrcType::SAB(ref slice_type) = s.src_type {
                    if slice_type.deref_type_id() == TypeId::of::<T>() {
                        return Some(i);
                    }
                }
                None
            }); //If None, it means we'll need to make our own output buffer

            let src = format!(
                "{struct_defn}__kernel void {program_name}({args}) {{\n{arg_setup}{pgrm}{out_cleanup}}}",
                struct_defn = struct_defn,
                program_name = program_name,
                args = ArgFormatter(
                    &self.sources[..],
                    output_buf_index.is_none(),
                    PhantomData::<T>
                ),
                arg_setup = ArgSetupFormatter(&self.sources[..]),
                pgrm = self.result_expr,
                out_cleanup =
                    OutCleanupFormatter(&self.sources[..], output_buf_index, PhantomData::<T>)
            );
            println!("Source:\n{}\n", src);

            let context_queue =
                crate::ocl_setup::default_cont_queue().ok_or(Error::ContextNotInitialized)?;
            let program = Program::builder().src(src).build(&context_queue.context)?;

            Ok(CachedProgram {
                program,
                program_name,
                context_queue,
                output_buf_index,
            })
        })?;

        let context_queue = &cached_program.context_queue;

        let mut kernel_builder = Kernel::builder();
        kernel_builder
            .program(&cached_program.program)
            .name(&cached_program.program_name)
            .queue(context_queue.queue.clone())
            .global_work_size(self.min_len);
        for s in self.sources.iter_mut() {
            match &mut s.src_type {
                SrcType::SAB(sab) => {
                    sab.create_buffer(&context_queue.context)?;
                    sab.add_to_kernel_builder(&mut kernel_builder);
                }
                _ => unimplemented!(),
                // SrcType::ScalarConst(val) => {
                //     val.add_to_kernel_builder(&mut kernel_builder);
                // }
            }
        }

        let explicit_output_buffer = if cached_program.output_buf_index.is_some() {
            None
        } else {
            let buf = Buffer::<T>::builder()
                .len(self.min_len)
                .context(&context_queue.context)
                .build()?;
            Some(buf)
        };
        explicit_output_buffer
            .as_ref()
            .map(|buf| kernel_builder.arg(buf));

        let kernel = kernel_builder.build()?;

        unsafe {
            kernel.enq()?;
        }

        let mut vec: Vec<T> = vec![Default::default(); self.min_len];

        if let Some(buf) = explicit_output_buffer {
            buf.read(&mut vec).queue(&context_queue.queue).enq()?
        } else if let Some(i) = cached_program.output_buf_index {
            if let SrcType::SAB(sab) = &self.sources[i].src_type {
                let buffer = sab
                    .get_buffer_as_any()
                    .downcast_ref::<Buffer<T>>()
                    .expect("Tried to find a buffer of matching type, but the type was different than expected.");
                buffer.read(&mut vec).queue(&context_queue.queue).enq()?
            }
        } else {
            panic!("One of output_buf_index or explicit_output_buffer should have been Some()");
        }

        Ok(vec)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    extern crate test;
    use test::Bencher;

    use crate::ocl_setup::ContextQueue;

    use ocl::{Buffer, Program};

    #[test]
    fn basic() -> Result<(), Error> {
        let v: Vec<i32> = vec![1, 2, 3, 4];
        let g = v.gpu_iter().map(|a| 9 * a);
        let out: Vec<i32> = g.collect()?;

        dbg!(&out);
        assert_eq!(out, [9, 18, 27, 36]);

        Ok(())
    }

    #[test]
    fn zipped() -> Result<(), Error> {
        let v = vec![1, 2, 3, 4];
        let v2 = vec![1, 2, 3, 4];

        let g = v.gpu_iter().map(|a| (&a + 2, &a + 3)); //.map(|(a, _b)| a + 3);
        let g2 = v2.gpu_iter().map(|a| a * 9);

        let zipped = g.zip(g2);
        let sum = zipped.map(|(a, b, c)| a + b + c);

        let out: Vec<i32> = sum.collect()?;

        dbg!(&out);
        assert_eq!(out, [16, 27, 38, 49]);

        Ok(())
    }

    #[test]
    fn one_line() -> Result<(), Error> {
        let out: Vec<i32> = vec![1, 2, 3, 4].gpu_iter().map(|a| a * 9).collect()?;

        assert_eq!(out, [9, 18, 27, 36]);

        Ok(())
    }

    #[test]
    fn raw_map() -> Result<(), Error> {
        let v: Vec<i32> = vec![1, 2, 3, 4];
        let g = v.gpu_iter().map(|a| 9 * a).raw_map::<u32, _>(|f, out, a| {
            write!(f, "{out} = {a} + 1; //comment", out = out, a = a)
        });
        let out = g.collect()?;

        dbg!(&out);
        assert_eq!(out, [10, 19, 28, 37]);

        Ok(())
    }

    #[test]
    fn raw_map_carry() -> Result<(), Error> {
        let v: Vec<i32> = vec![1, 2, 3, 4];
        let g = v
            .gpu_iter()
            .map(|a| 9 * a)
            .raw_map_carry::<i32, _>(|f, out, a| {
                write!(f, "{out} = {a} + 1; //comment", out = out, a = a)
            })
            .map(|(r, a)| r - a);
        let out = g.collect()?;

        dbg!(&out);
        assert_eq!(out, [1, 1, 1, 1]);

        Ok(())
    }

    #[bench]
    fn map_collect(b: &mut Bencher) -> Result<(), Error> {
        b.iter(|| vec![1, 2, 3, 4].gpu_iter().map(|a| a * 9).collect());
        Ok(())
    }

    fn to_celsius(faren: Expr<i32>) -> Expr<i32> {
        ((faren.cast_unchecked::<f32>() - 32.0) / 1.8).round()
    }

    fn no_cache(b: &mut Bencher, data: &[i32]) {
        b.iter(|| {
            cache::COMPILE_CACHE.write().unwrap().clear();
            data.gpu_iter().map(to_celsius).collect()
        });
    }

    fn with_cache(b: &mut Bencher, data: &[i32]) {
        let _ = data.gpu_iter().map(to_celsius).collect();
        b.iter(|| {
            let _ = cache::COMPILE_CACHE.write().unwrap();
            data.gpu_iter().map(to_celsius).collect()
        });
    }

    fn large_load() -> Vec<i32> {
        vec![1, 2, 3, 4].into_iter().cycle().take(1 << 24).collect() // 32MB worth of data.
    }

    #[bench]
    fn bench_no_cache_small(b: &mut Bencher) {
        no_cache(b, &[1, 2, 3, 4]);
    }

    #[bench]
    fn bench_with_cache_small(b: &mut Bencher) {
        with_cache(b, &[1, 2, 3, 4]);
    }

    #[bench]
    fn bench_no_cache_large(b: &mut Bencher) {
        no_cache(b, &large_load());
    }

    #[bench]
    fn bench_with_cache_large(b: &mut Bencher) {
        with_cache(b, &large_load());
    }

    fn opencl_setup() -> (ContextQueue, Program) {
        let src = r#"
            __kernel void mul(__global int* buffer0) {
                int var0 = buffer0[get_global_id(0)];
                int __gpu_iter_result = round((((float)var0 - (float)32) / (float)1.8));
                buffer0[get_global_id(0)] = (int)__gpu_iter_result;
            }
        "#;

        let context_queue = ContextQueue::with_best_device().unwrap();

        let program = Program::builder()
            .src(src)
            .build(&context_queue.context)
            .unwrap();

        (context_queue, program)
    }

    fn opencl_no_cache(b: &mut Bencher, data: &[i32]) {
        b.iter(|| {
            let (context_queue, program) = opencl_setup();

            let buffer = Buffer::<i32>::builder()
                .len(data.len())
                .queue(context_queue.queue.clone())
                .copy_host_slice(&data)
                .build()
                .unwrap();

            let kernel = Kernel::builder()
                .program(&program)
                .name("mul")
                .queue(context_queue.queue.clone())
                .global_work_size(data.len())
                .arg(&buffer)
                .build()
                .unwrap();

            unsafe {
                kernel.enq().unwrap();
            }

            let mut out = vec![0; buffer.len()];
            buffer.read(&mut out).enq().unwrap();

            let _ = cache::COMPILE_CACHE.write().unwrap();
            out
        });
    }

    fn opencl_with_cache(b: &mut Bencher, data: &[i32]) {
        let (context_queue, program) = opencl_setup();

        b.iter(|| {
            let buffer = Buffer::<i32>::builder()
                .len(data.len())
                .queue(context_queue.queue.clone())
                .copy_host_slice(&data)
                .build()
                .unwrap();

            let kernel = Kernel::builder()
                .program(&program)
                .name("mul")
                .queue(context_queue.queue.clone())
                .global_work_size(data.len())
                .arg(&buffer)
                .build()
                .unwrap();

            unsafe {
                kernel.enq().unwrap();
            }

            let mut out = vec![0; buffer.len()];
            buffer.read(&mut out).enq().unwrap();

            let _ = cache::COMPILE_CACHE.write().unwrap();
            out
        });
    }

    #[bench]
    fn bench_opencl_no_cache_small(b: &mut Bencher) {
        opencl_no_cache(b, &[1, 2, 3, 4])
    }

    #[bench]
    fn bench_opencl_with_cache_small(b: &mut Bencher) {
        opencl_with_cache(b, &[1, 2, 3, 4])
    }

    #[bench]
    fn bench_opencl_no_cache_large(b: &mut Bencher) {
        opencl_no_cache(b, &large_load())
    }

    #[bench]
    fn bench_opencl_with_cache_large(b: &mut Bencher) {
        opencl_with_cache(b, &large_load())
    }

    #[bench]
    fn bench_cpu(b: &mut Bencher) {
        let load = large_load();

        b.iter(|| {
            load.iter()
                .map(|faren| (((*faren as f32) - 32.0) / 1.8).round() as i32)
                .collect::<Vec<i32>>()
        })
    }
}
