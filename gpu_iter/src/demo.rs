struct GPUIterator;
struct GPUIteratorMut;

trait IntoGPUIterator {
    fn gpu_iter(&self) -> GPUIterator; // ocl::flags::READ
    fn gpu_iter_mut(&self) -> GPUIteratorMut; // ocl::flags::READ_WRITE
}

#[derive(ToGPU)]
struct Foobar {
    a: i32,
    b: u8,
}

#[cfg(test)]
mod tests {
    #[test]
    fn map_with_struct() {
        let input = [ Foobar{a: 12_i32, b: 4_u8}, Foobar{a: 100_i32, b: 8_u8}, Foobar{a: -10_i32, b: 3_u8} ];
        let result: Vec<i32> = input.gpu_iter()
            .map(|f| f.a * f.b)
            .sum();

        assert_eq!(result, 12 * 4 + 100 * 8 - 10 * 3);
    }

    #[test]
    fn map_collect() {
        let input = [1, 2, 3, 4];
        let result: Vec<i32> = input.gpu_iter()
            .map(|i| i * i)
            .collect();

        assert_eq!(result, vec!(1, 4, 9, 16));
    }

    #[test]
    fn map_zip() {
        let input = [1, 2, 3, 4];
        let other = input.gpu_iter().map(|i| i + 10);
        let result: Vec<i32> = input.gpu_iter()
            .map(|i| i * i)
            .zip(other)
            .map(|a, b| a += 2)
            .collect();

        assert_eq!(result, vec!(1, 4, 9, 16));
    }

    #[test]
    fn map_sum() {
        let input = [1, 2, 3, 4];
        let result = input.gpu_iter()
            .map(|i| i * i)
            .sum();

        assert_eq!(result, 30);
    }

    #[test]
    fn map_reduce() {
        let input = [(0, 1), (5, 6), (16, 2), (8, 9)];
        let result = input.gpu_iter()
            .map(|i| i * -1)
            .reduce(
                || (0, 0), // the "identity" is 0 in both columns
                |a, b| (a.0 + b.0, a.1 + b.1));

        assert_eq!(result, (0 - 5 - 16 - 8, -1 - 6 - 2 - 9));
    }

    #[test]
    fn map_with() {
        let input = [1, 2, 3, 4];
        let result = input.gpu_iter()
            .map_with(42, |given_num, i|
                given_num * i
            ).sum();

        assert_eq!(result, 1 * 42 + 2 * 42 + 3 * 42 + 4 * 42);
    }

    #[test]
    fn geometry_ops() {
        use gpu_iter::OCL::{vec2, normalize, dot};

        let input = [vec2!(1, 2), vec2!(3, 4), vec2!(5, 6)]]];
        let result1 = input.gpu_iter()
            .map(|v| dot(normalize(v), vec2!(0, 1)))
            .sum();

        let result2 = input.gpu_iter()
            .map(|v| {
                let n = normalize(v);
                dot(n, vec2!(0, 1)))
            }
            .sum();

        assert_eq!(result1, result2);
    }

    #[test]
    fn map_ifs() {
        let input = [-1, -2, 3, 4];
        let result: Vec<i32> = input.gpu_iter()
            .map(|i|
                i.gt(0, i * 2).els(i)
            ).sum();

        assert_eq!(result, -1 - 2 + 3 * 2 + 4 * 2);
    }

    #[test]
    fn map_src() {
        let input = [1, 2, 3, 4];
        let result = input.gpu_iter()
            .map_src("v", r#"
                if v > 0 {
                    v *= 2;
                } else {
                    v
                }
            "#)
            .sum();

        assert_eq!(result, -1 - 2 + 3 * 2 + 4 * 2);
    }

    #[test]
    fn map_src_full() {
        let input = [1, 2, 3, 4];
        let result = input.gpu_iter()
            .map_src_full("add", r#"
                __kernel void add(__global float* buffer) {
                    buffer[get_global_id(0)] += 3;
                }
            "#)
            .sum();

        assert_eq!(result, -1 - 2 + 3 * 2 + 4 * 2);
    }

    #[test]
    fn map_with_fun() {
        let input = [1, 2, 3, 4];
        let result = input.gpu_iter()
            .with_fun(r#"
                float foobar(float val) {
                    val * 6;
                }
            "#)
            .map(|i| cl_fn!(foobar(i + 2)))
            .sum();

        assert_eq!(result, -1 - 2 + 3 * 2 + 4 * 2);
    }
}
