use failure::Fail;

#[derive(Debug, Fail)]
pub enum Error {
    #[fail(
        display = "Global ContextQueue failed to initialize. Set with gpu_iter::ocl_setup::set_cont_queue(...)"
    )]
    ContextNotInitialized,
    #[fail(display = "{}", _0)]
    DeviceLookupError(#[cause] DeviceLookupError),
    #[fail(display = "{}", _0)]
    OclError(#[cause] ::ocl::Error),
}

impl From<ocl::Error> for Error {
    fn from(err: ocl::Error) -> Self {
        Self::OclError(err)
    }
}

#[derive(Debug, Fail)]
pub enum DeviceLookupError {
    #[fail(display = "No GPUs or accelerators found")]
    NoGpusOrAcceleratorsFound,
    #[fail(display = "No system default device found")]
    NoSystemDefaultDeviceFound,
    #[fail(display = "{}", _0)]
    OclError(#[cause] ::ocl::Error),
}

impl From<ocl::Error> for DeviceLookupError {
    fn from(err: ocl::Error) -> Self {
        Self::OclError(err)
    }
}

impl From<DeviceLookupError> for Error {
    fn from(err: DeviceLookupError) -> Self {
        if let DeviceLookupError::OclError(ocl_err) = err {
            Error::OclError(ocl_err)
        } else {
            Self::DeviceLookupError(err)
        }
    }
}
