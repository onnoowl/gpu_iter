use std::fmt;
use std::marker::PhantomData;

use num_complex::{Complex32, Complex64};
use ocl::prm::*;

pub struct TypeRenderer<T: RenderTypeStr>(PhantomData<T>);
pub struct DynTypeRenderer<'a>(&'a dyn DynRenderTypeStr);

impl<T: RenderTypeStr> fmt::Display for TypeRenderer<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> fmt::Result {
        T::impl_render(f)
    }
}

impl<'a> fmt::Display for DynTypeRenderer<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> fmt::Result {
        self.0.impl_render(f)
    }
}

pub trait RenderTypeStr: fmt::Debug + 'static {
    // In order for trait objects with this to be created, get_type_str must take a "self" arg.
    fn render_type() -> TypeRenderer<Self>
    where
        Self: Sized,
    {
        TypeRenderer(PhantomData)
    }
    fn impl_render(f: &mut fmt::Formatter<'_>) -> fmt::Result;
    fn impl_render_struct_defn(_f: &mut fmt::Formatter<'_>) -> fmt::Result;
    fn needs_struct_defn() -> bool;
}

pub trait DynRenderTypeStr {
    // In order for trait objects with this to be created, get_type_str must take a "self" arg.
    fn render_type(&self) -> DynTypeRenderer<'_>
    where
        Self: std::marker::Sized, //TODO: Why does this need Sized?
    {
        DynTypeRenderer(self)
    }
    fn impl_render(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result;
    fn impl_render_struct_defn(&self, _f: &mut fmt::Formatter<'_>) -> fmt::Result;
    fn needs_struct_defn(&self) -> bool;
}

impl<T: RenderTypeStr> DynRenderTypeStr for T {
    fn impl_render(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        T::impl_render(f)
    }
    fn impl_render_struct_defn(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        T::impl_render_struct_defn(f)
    }
    fn needs_struct_defn(&self) -> bool {
        T::needs_struct_defn()
    }
}

macro_rules! get_str_type_impl {
    ($($t:ty : $s:expr),*) => ($(
        impl RenderTypeStr for $t {
            fn impl_render(f: &mut fmt::Formatter<'_>) -> fmt::Result {
                write!(f, $s)
            }
            fn impl_render_struct_defn(_f: &mut fmt::Formatter<'_>) -> fmt::Result {
                Ok(())
            }
            fn needs_struct_defn() -> bool {
                false
            }
        }
    )*)
}
get_str_type_impl! {
    bool:"bool", i8:"char", u8:"uchar", i16:"short", u16:"ushort", i32:"int", u32:"uint", i64:"long", u64:"ulong", f32:"float", f64:"double",
    Complex32:"complex float", Complex64:"complex double",
    Char:"char", Uchar:"uchar", Short:"short", Ushort:"ushort", Int:"int", Uint:"uint", Long:"long", Ulong:"ulong", Float:"float", Double:"double"
}
