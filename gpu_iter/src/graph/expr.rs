use super::*;

use std::any::TypeId;
use std::collections::hash_map::HashMap;
use std::fmt;
use std::marker::PhantomData;

use smallvec::smallvec;

pub(crate) const VAR_PREFIX: &str = "__gpu_iter";

#[derive(fmt::Debug)]
pub struct Expr<T: RenderTypeStr> {
    pub(crate) node: DynNode,
    _pht: PhantomData<T>,
}

impl<T: RenderTypeStr> Expr<T> {
    /// Do not use directly. This is `pub` so that it can be used in  #![derive(ToGpu)] and vector!() macro expansions.
    pub fn new<N>(inner: N) -> Self
    where
        N: Node + ResolvesToType<Type = T>,
    {
        Expr {
            node: DynNode::new(inner),
            _pht: PhantomData,
        }
    }

    pub(crate) fn with_node(node: DynNode) -> Self {
        Expr {
            node,
            _pht: PhantomData,
        }
    }

    pub fn node(&self) -> DynNode {
        self.node.clone()
    }

    pub fn cast<C: RenderTypeStr>(&self) -> Expr<C>
    where
        C: From<T>,
    {
        Expr::new(Cast::new(self.node.clone()))
    }

    pub fn cast_unchecked<C: RenderTypeStr>(&self) -> Expr<C> {
        Expr::new(Cast::new(self.node.clone()))
    }

    // TODO: Make these types only work for numerics
    pub fn abs(&self) -> Expr<T> {
        Expr::new(Function::new("abs", smallvec!(self.node.clone())))
    }

    pub fn max<O>(&self, other: O) -> Expr<T>
    where
        O: IntoExpr<Type = T>,
    {
        Expr::new(Function::new(
            "max",
            smallvec!(self.node.clone(), other.into_expr().node),
        ))
    }

    pub fn min<O>(&self, other: O) -> Expr<T>
    where
        O: IntoExpr<Type = T>,
    {
        Expr::new(Function::new(
            "min",
            smallvec!(self.node.clone(), other.into_expr().node),
        ))
    }

    fn build_meta_map(&self) -> HashMap<u64, NodeMetaData> {
        let mut meta_map = HashMap::new();

        self.node.build_meta_map(&mut meta_map, 0, &mut 0, true);

        meta_map
    }

    pub(crate) fn write_struct_defns(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut hash_map: HashMap<TypeId, DynNode> = HashMap::new();

        self.node.collect_types_needing_struct_defns(&mut hash_map);

        hash_map
            .iter()
            .map(|(_, node)| node.impl_render_struct_defn(f))
            .collect::<fmt::Result>()?;

        Ok(())
    }
}

impl Expr<bool> {
    pub fn then<Then>(&self, then: Then) -> IfBuilderNeedsElse<Then::Type>
    where
        Then: IntoExpr,
    {
        if_then(self.clone(), then)
    }

    pub fn then_els<Then, Els>(&self, then: Then, els: Els) -> Expr<Then::Type>
    where
        Then: IntoExpr,
        Els: IntoExpr<Type = Then::Type>,
    {
        if_then_els(self.clone(), then, els)
    }
}

impl Expr<f32> {
    pub fn round(&self) -> Expr<i32> {
        Expr::new(Function::new("round", smallvec!(self.node.clone())))
    }
}

impl Expr<f64> {
    pub fn round(&self) -> Expr<i64> {
        Expr::new(Function::new("round", smallvec!(self.node.clone())))
    }
}

impl<T: RenderTypeStr> fmt::Display for Expr<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let meta_map = self.build_meta_map();

        // First we'll render expressions used repeatedly.
        // We'll render these first, and put them as variable assignments in the beginning.
        // We'll find these variables by looking at the metadata from traversing the graph.
        let mut metas_with_reuse: Vec<&NodeMetaData> = meta_map
            .values()
            .filter(|meta| meta.num_parents > 1 || meta.expr.inner().is_statement())
            .collect();

        metas_with_reuse.sort_by_key(|meta| std::u32::MAX - meta.max_graph_level);
        for meta in metas_with_reuse.into_iter() {
            let var_num = meta
                .var_num
                .expect("Variable failed to have var_num assigned by graph traversal.");
            let var_type = meta.expr.render_type();
            let rend = meta.expr.clone().direct_renderer(&meta_map);

            write!(f, "{} {}_var{}", var_type, VAR_PREFIX, var_num)?;
            if !meta.expr.inner().is_statement() {
                writeln!(f, " = {};", rend)?;
            } else {
                writeln!(f, ";\n{}", rend)?; //since it's a statement, it will fill in the value later.
            }
        }
        writeln!(
            f,
            "{} {}_result = {};",
            &self.node.render_type(),
            VAR_PREFIX,
            self.node.clone().renderer(&meta_map)
        )
    }
}

impl<T: RenderTypeStr> Clone for Expr<T> {
    fn clone(&self) -> Self {
        Expr::with_node(self.node.clone())
    }
}

// Into<Expr<Inner>> is not sufficient.
// We need an "Into" trait with an associated inner type, rather than a generic one.
pub trait IntoExpr {
    type Type: RenderTypeStr;

    fn into_expr(self) -> Expr<Self::Type>;
}

pub trait NonReflexiveIntoExpr: IntoExpr {}

impl<T: RenderTypeStr> IntoExpr for Expr<T> {
    type Type = T;

    fn into_expr(self) -> Expr<Self::Type> {
        self
    }
}

impl<T: RenderTypeStr> IntoExpr for &Expr<T> {
    type Type = T;

    fn into_expr(self) -> Expr<Self::Type> {
        self.clone()
    }
}
// Since Rust doesn't have negative trait bounds (yet), we need to impl this for all T: IntoExpr where T != Expr<_>
impl<T: RenderTypeStr> NonReflexiveIntoExpr for &Expr<T> {}

impl<T: RenderTypeStr, E: IntoExpr<Type = T> + NonReflexiveIntoExpr> From<E> for Expr<T> {
    fn from(e: E) -> Self {
        e.into_expr()
    }
}
