use crate::get_type_str::RenderTypeStr;

use std::fmt;

pub trait ResolvesToType {
    type Type: RenderTypeStr;
}

impl<T: ResolvesToType + fmt::Debug + 'static> RenderTypeStr for T {
    fn impl_render(f: &mut fmt::Formatter<'_>) -> fmt::Result {
        T::Type::impl_render(f)
    }
    fn impl_render_struct_defn(f: &mut fmt::Formatter<'_>) -> fmt::Result {
        T::Type::impl_render_struct_defn(f)
    }
    fn needs_struct_defn() -> bool {
        T::Type::needs_struct_defn()
    }
}
