use super::*;

use std::collections::HashMap;
use std::fmt;

pub struct Renderer<'s> {
    pub(crate) dyn_node: DynNode,
    pub(crate) meta_map: &'s HashMap<u64, NodeMetaData>,
    pub(crate) direct: bool,
}

impl fmt::Display for Renderer<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let key = self.dyn_node.default_ptr_hash();
        let meta = self
            .meta_map
            .get(&key)
            .expect("MetaMap entry failed to get computed for expression.");
        if !self.direct {
            if let Some(var_num) = meta.var_num {
                return write!(f, "{}_var{}", VAR_PREFIX, var_num);
            }
        }
        self.dyn_node
            .inner()
            .render_with(f, self.dyn_node.default_ptr_hash(), self.meta_map)
    }
}
