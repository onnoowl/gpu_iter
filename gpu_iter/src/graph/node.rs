use super::*;

use std::any::TypeId;
use std::collections::{hash_map::DefaultHasher, HashMap};
use std::fmt;
use std::hash::Hasher;
use std::sync::Arc;

pub trait Node: DynRenderTypeStr + fmt::Debug + 'static {
    fn call_on_children(&self, fun: &mut dyn FnMut(&[DynNode]));
    fn render_with(
        &self,
        f: &mut fmt::Formatter<'_>,
        self_key: u64,
        meta_map: &HashMap<u64, NodeMetaData>,
    ) -> fmt::Result;
    fn is_statement(&self) -> bool {
        false
    }
    fn get_type_id(&self) -> TypeId {
        TypeId::of::<Self>()
    }
}

#[derive(Clone, Debug)]
pub struct DynNode(Arc<dyn Node>);

impl DynNode {
    pub(crate) fn new<N: Node>(inner: N) -> Self {
        DynNode(Arc::new(inner))
    }

    pub(crate) fn inner(&self) -> &Arc<dyn Node> {
        &self.0
    }

    pub fn renderer<'s>(self, meta_map: &'s HashMap<u64, NodeMetaData>) -> Renderer<'s> {
        Renderer {
            dyn_node: self,
            meta_map,
            direct: false,
        }
    }
    pub(crate) fn direct_renderer<'s>(
        self,
        meta_map: &'s HashMap<u64, NodeMetaData>,
    ) -> Renderer<'s> {
        Renderer {
            dyn_node: self,
            meta_map,
            direct: true,
        }
    }

    pub(crate) fn default_ptr_hash(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        let ptr = Arc::into_raw(self.0.clone());
        std::ptr::hash(ptr, &mut hasher);
        hasher.finish()
    }

    pub(crate) fn build_meta_map(
        &self,
        meta_map: &mut HashMap<u64, NodeMetaData>,
        depth: u32,
        var_counter: &mut u32,
        first_pass: bool,
    ) {
        let entry = meta_map
            .entry(self.default_ptr_hash())
            .or_insert_with(|| NodeMetaData::new(self.clone()));
        entry.max_graph_level = entry.max_graph_level.max(depth);

        let mut first_pass = first_pass; //make it mutable
        if first_pass {
            entry.num_parents += 1;
            if entry.num_parents > 1 || self.inner().is_statement() {
                if entry.var_num.is_none() {
                    entry.var_num = Some(*var_counter);
                    *var_counter += 1;
                }
                if entry.num_parents > 1 {
                    first_pass = false; //someone's been here before, this is no longer a first pass
                }
            }
        }

        self.0.call_on_children(&mut |children: &[DynNode]| {
            for c in children.iter() {
                c.build_meta_map(meta_map, depth + 1, var_counter, first_pass)
            }
        });
    }

    pub(crate) fn collect_types_needing_struct_defns(
        &self,
        hash_map: &mut HashMap<TypeId, DynNode>,
    ) {
        if self.0.needs_struct_defn() {
            hash_map
                .entry(self.0.get_type_id())
                .or_insert_with(|| self.clone());
        }

        self.0.call_on_children(&mut |children: &[DynNode]| {
            for c in children.iter() {
                c.collect_types_needing_struct_defns(hash_map);
            }
        })
    }
}

impl DynRenderTypeStr for DynNode {
    fn impl_render(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.impl_render(f)
    }
    fn impl_render_struct_defn(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.impl_render_struct_defn(f)
    }
    fn needs_struct_defn(&self) -> bool {
        self.0.needs_struct_defn()
    }
}

#[derive(Debug)]
pub struct NodeMetaData {
    pub(crate) var_num: Option<u32>,
    pub(crate) max_graph_level: u32,
    pub(crate) num_parents: u32,
    pub(crate) expr: DynNode,
}

impl NodeMetaData {
    pub(crate) fn new(expr: DynNode) -> Self {
        NodeMetaData {
            var_num: None,
            max_graph_level: 0,
            num_parents: 0,
            expr,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn dyn_node_hashes_equal() {
        let e: Expr<_> = 3i32.into();
        let d1: DynNode = e.clone().node;
        let d2: DynNode = d1.clone();
        let d3: DynNode = e.clone().node;
        let d4: DynNode = Expr::<i32>::with_node(e.node.clone()).node;
        assert_eq!(d1.default_ptr_hash(), d2.default_ptr_hash());
        assert_eq!(d2.default_ptr_hash(), d3.default_ptr_hash());
        assert_eq!(d3.default_ptr_hash(), d4.default_ptr_hash());
    }
}
