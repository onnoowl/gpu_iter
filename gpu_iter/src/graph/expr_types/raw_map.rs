use super::*;
use crate::gpu_iter::ExprOrTuple;

use std::collections::HashMap;
use std::fmt;
use std::marker::PhantomData;

pub struct RawMap<
    OUT: RenderTypeStr,
    IN: ExprOrTuple + 'static,
    F: Fn(&mut fmt::Formatter<'_>, VarName, IN::VarNames) -> fmt::Result + 'static,
> {
    in_dyn_nodes: IN::DynNodes,
    fun: F,
    _pht: PhantomData<OUT>,
}

pub struct VarName {
    pub(crate) var_num: u32,
}

impl fmt::Display for VarName {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{prefix}_var{var_num}",
            prefix = VAR_PREFIX,
            var_num = self.var_num
        )
    }
}

impl<OUT, IN, F> RawMap<OUT, IN, F>
where
    OUT: RenderTypeStr,
    IN: ExprOrTuple,
    F: Fn(&mut fmt::Formatter<'_>, VarName, IN::VarNames) -> fmt::Result,
{
    pub fn new(expr_or_tuple: IN, fun: F) -> Self {
        RawMap {
            in_dyn_nodes: expr_or_tuple.to_dyn_nodes(),
            fun,
            _pht: PhantomData,
        }
    }
}

impl<OUT, IN, F> Node for RawMap<OUT, IN, F>
where
    OUT: RenderTypeStr,
    IN: ExprOrTuple + 'static,
    for<'r, 's> F: Fn(&'r mut fmt::Formatter<'s>, VarName, IN::VarNames) -> fmt::Result + 'static,
{
    fn call_on_children(&self, fun: &mut dyn FnMut(&[DynNode])) {
        // Call twice to indicate to system that vars could be reused, and should be factored out.
        // TODO: Better solution needed
        IN::call_on_dyn_nodes(self.in_dyn_nodes.clone(), fun);
        IN::call_on_dyn_nodes(self.in_dyn_nodes.clone(), fun);
    }
    fn render_with(
        &self,
        f: &mut fmt::Formatter<'_>,
        self_key: u64,
        meta_map: &HashMap<u64, NodeMetaData>,
    ) -> fmt::Result {
        let var_num = meta_map
            .get(&self_key)
            .expect("Rawmap failed to get a meta entry created during graph traversal.")
            .var_num
            .expect("Rawmap failed to have var_num assigned by graph traversal.");
        write!(f, "{{\n    ")?;
        (&self.fun)(
            f,
            VarName { var_num },
            IN::get_var_names(self.in_dyn_nodes.clone(), meta_map),
        )?;
        write!(f, "\n}}")
    }

    // "if({cond}) {{\n    {prefix}_var{var_num} = {then};\n}} else {{\n    {prefix}_var{var_num} = {els};\n}}",

    fn is_statement(&self) -> bool {
        true
    }
}

impl<OUT, IN, F> ResolvesToType for RawMap<OUT, IN, F>
where
    OUT: RenderTypeStr,
    IN: ExprOrTuple,
    F: Fn(&mut fmt::Formatter<'_>, VarName, IN::VarNames) -> fmt::Result,
{
    type Type = OUT;
}

impl<OUT, IN, F> fmt::Debug for RawMap<OUT, IN, F>
where
    OUT: RenderTypeStr,
    IN: ExprOrTuple,
    F: Fn(&mut fmt::Formatter<'_>, VarName, IN::VarNames) -> fmt::Result,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "RawMap {{ in_dyn_nodes: {:?} }}", self.in_dyn_nodes)
    }
}
