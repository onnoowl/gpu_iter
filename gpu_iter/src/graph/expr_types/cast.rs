use super::*;

use std::collections::HashMap;
use std::fmt;
use std::marker::PhantomData;

#[derive(Debug)]
pub struct Cast<T: RenderTypeStr> {
    inner: DynNode,
    _pht: PhantomData<T>,
}

impl<T: RenderTypeStr> Cast<T> {
    pub(crate) fn new(inner: DynNode) -> Self {
        Cast {
            inner,
            _pht: PhantomData,
        }
    }
}

impl<T: RenderTypeStr> Node for Cast<T> {
    fn call_on_children(&self, fun: &mut dyn FnMut(&[DynNode])) {
        fun(&[self.inner.clone()])
    }

    fn render_with(
        &self,
        f: &mut fmt::Formatter<'_>,
        _self_key: u64,
        meta_map: &HashMap<u64, NodeMetaData>,
    ) -> fmt::Result {
        write!(
            f,
            "({}){}",
            T::render_type(),
            self.inner.clone().renderer(meta_map)
        )
    }
}

impl<T: RenderTypeStr> ResolvesToType for Cast<T> {
    type Type = T;
}
