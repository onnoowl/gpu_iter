use super::*;

use std::collections::HashMap;
use std::fmt;
use std::marker::PhantomData;

#[derive(Debug)]
pub struct UnaryExpr<T: RenderTypeStr> {
    op_type: UnaryOpType,
    inner: DynNode,
    _pht: PhantomData<T>,
}

impl<T: RenderTypeStr> UnaryExpr<T> {
    pub(crate) fn new<Output: RenderTypeStr>(op_type: UnaryOpType, inner: DynNode) -> Self {
        UnaryExpr {
            op_type,
            inner,
            _pht: PhantomData,
        }
    }
}

impl<Output: RenderTypeStr> Node for UnaryExpr<Output> {
    fn call_on_children(&self, fun: &mut dyn FnMut(&[DynNode])) {
        fun(&[self.inner.clone()])
    }

    fn render_with(
        &self,
        f: &mut fmt::Formatter<'_>,
        _self_key: u64,
        meta_map: &HashMap<u64, NodeMetaData>,
    ) -> fmt::Result {
        write!(
            f,
            "{}{}",
            self.op_type,
            self.inner.clone().renderer(meta_map)
        )
    }
}

impl<T: RenderTypeStr> ResolvesToType for UnaryExpr<T> {
    type Type = T;
}

#[derive(Clone, Debug, PartialEq)]
pub enum UnaryOpType {
    Neg,
    Not,
}

impl fmt::Display for UnaryOpType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use UnaryOpType::*;

        write!(
            f,
            "{}",
            match self {
                Neg => "-",
                Not => "!",
            }
        )
    }
}

macro_rules! ops_impl {
    ($($t:tt ; $e:expr ; $m:tt),*) => ($(
        impl<L, O> $t for Expr<L>
        where
            L: RenderTypeStr + $t<Output=O>,
            O: RenderTypeStr,
        {
            type Output = Expr<UnaryExpr<O>>;

            fn $m(self) -> Self::Output {
                Expr::new(UnaryExpr::new::<O>($e, self.node))
            }
        }
    )*)
}

use std::ops::{Neg, Not};

ops_impl! {
    Neg ; UnaryOpType::Neg ; neg,
    Not ; UnaryOpType::Not ; not
}
