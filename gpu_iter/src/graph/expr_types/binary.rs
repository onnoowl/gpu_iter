use super::*;

use std::collections::HashMap;
use std::fmt;
use std::marker::PhantomData;

use num_complex::{Complex32, Complex64};

#[derive(Debug)]
pub struct BinaryExpr<Output: RenderTypeStr> {
    op_type: BinaryOpType,
    left: DynNode,
    right: DynNode,
    _pht: PhantomData<Output>,
}

impl<T: RenderTypeStr> BinaryExpr<T> {
    pub(crate) fn new<Output: RenderTypeStr>(
        op_type: BinaryOpType,
        left: DynNode,
        right: DynNode,
    ) -> Self {
        BinaryExpr {
            op_type,
            left,
            right,
            _pht: PhantomData,
        }
    }
}

impl<Output: RenderTypeStr> Node for BinaryExpr<Output> {
    fn call_on_children(&self, fun: &mut dyn FnMut(&[DynNode])) {
        fun(&[self.left.clone(), self.right.clone()])
    }

    fn render_with(
        &self,
        f: &mut fmt::Formatter<'_>,
        _self_key: u64,
        meta_map: &HashMap<u64, NodeMetaData>,
    ) -> fmt::Result {
        write!(
            f,
            "({} {} {})",
            self.left.clone().renderer(meta_map),
            self.op_type,
            self.right.clone().renderer(meta_map)
        )
    }
}

impl<T: RenderTypeStr> ResolvesToType for BinaryExpr<T> {
    type Type = T;
}

#[derive(Clone, Debug, PartialEq)]
pub enum BinaryOpType {
    Add,
    BitAnd,
    BitOr,
    BitXor,
    Div,
    Gt,
    Lt,
    Eq,
    Mul,
    Ne,
    Rem,
    Shl,
    Shr,
    Sub,
}

impl fmt::Display for BinaryOpType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use BinaryOpType::*;

        write!(
            f,
            "{}",
            match self {
                Add => "+",
                BitAnd => "&",
                BitOr => "|",
                BitXor => "^",
                Eq => "==",
                Div => "/",
                Gt => ">",
                Lt => "<",
                Mul => "*",
                Ne => "!=",
                Rem => "%",
                Shl => "<<",
                Shr => ">>",
                Sub => "-",
            }
        )
    }
}

pub trait Cmp {
    type Type: RenderTypeStr;

    fn eq<R>(self, right: R) -> Expr<bool>
    where
        R: IntoExpr<Type = Self::Type>;

    fn ne<R>(self, right: R) -> Expr<bool>
    where
        R: IntoExpr<Type = Self::Type>;

    fn gt<R>(self, right: R) -> Expr<bool>
    where
        R: IntoExpr<Type = Self::Type>;

    fn lt<R>(self, right: R) -> Expr<bool>
    where
        R: IntoExpr<Type = Self::Type>;
}

impl<L> Cmp for L
where
    L: IntoExpr,
{
    type Type = L::Type;

    fn eq<R>(self, right: R) -> Expr<bool>
    where
        R: IntoExpr<Type = Self::Type>,
    {
        Expr::new(BinaryExpr::new::<bool>(
            BinaryOpType::Eq,
            self.into_expr().node,
            right.into_expr().node,
        ))
    }

    fn ne<R>(self, right: R) -> Expr<bool>
    where
        R: IntoExpr<Type = Self::Type>,
    {
        Expr::new(BinaryExpr::new::<bool>(
            BinaryOpType::Ne,
            self.into_expr().node,
            right.into_expr().node,
        ))
    }

    fn gt<R>(self, right: R) -> Expr<bool>
    where
        R: IntoExpr<Type = Self::Type>,
    {
        Expr::new(BinaryExpr::new::<bool>(
            BinaryOpType::Gt,
            self.into_expr().node,
            right.into_expr().node,
        ))
    }

    fn lt<R>(self, right: R) -> Expr<bool>
    where
        R: IntoExpr<Type = Self::Type>,
    {
        Expr::new(BinaryExpr::new::<bool>(
            BinaryOpType::Lt,
            self.into_expr().node,
            right.into_expr().node,
        ))
    }
}

// Implement the std::ops (things like +, -, /, *) for
// Expr on the left, and anything that can turn into an Expr on the right.
macro_rules! ops_impl {
    ($($t:tt ; $e:expr ; $m:tt),*) => ($(
        impl<LType, R, O> $t<R> for Expr<LType>
        where
            R: IntoExpr,
            LType: RenderTypeStr + $t<R::Type, Output=O>,
            O: RenderTypeStr,
        {
            type Output = Expr<O>;

            fn $m(self, right: R) -> Self::Output {
                Expr::new(BinaryExpr::new::<O>($e, self.node, right.into_expr().node))
            }
        }

        impl<LType, R, O> $t<R> for &Expr<LType>
        where
            R: IntoExpr,
            LType: RenderTypeStr + $t<R::Type, Output=O>,
            O: RenderTypeStr,
        {
            type Output = Expr<O>;

            fn $m(self, right: R) -> Self::Output {
                Expr::new(BinaryExpr::new::<O>($e, self.clone().node, right.into_expr().node))
            }
        }
    )*)
}

// Implement the std::ops (things like +, -, /, *) for
// T: Primitive on the left, and Expr<T> on the right.
// Assumes that the primitive on the left, the resolution type on the right, and the output type are all the same
macro_rules! impl_binary_for_prim_on_left {
    ($t:tt ; $e:expr ; $m:tt ; $($p:ty),*) => ($(
        impl $t<Expr<$p>> for $p
        where
            $p: $t<$p, Output=$p>
        {
            type Output = Expr<$p>;

            fn $m(self, right: Expr<$p>) -> Self::Output {
                Expr::new(BinaryExpr::new::<$p>($e, self.into_expr().node, right.node))
            }
        }

        impl $t<Expr<$p>> for &$p
        where
            $p: $t<$p, Output=$p>
        {
            type Output = Expr<$p>;

            fn $m(self, right: Expr<$p>) -> Self::Output {
                Expr::new(BinaryExpr::new::<$p>($e, self.into_expr().node, right.node))
            }
        }
        impl $t<&Expr<$p>> for $p
        where
            $p: $t<$p, Output=$p>
        {
            type Output = Expr<$p>;

            fn $m(self, right: &Expr<$p>) -> Self::Output {
                Expr::new(BinaryExpr::new::<$p>($e, self.into_expr().node, right.clone().node))
            }
        }

        impl $t<&Expr<$p>> for &$p
        where
            $p: $t<$p, Output=$p>
        {
            type Output = Expr<$p>;

            fn $m(self, right: &Expr<$p>) -> Self::Output {
                Expr::new(BinaryExpr::new::<$p>($e, self.into_expr().node, right.clone().node))
            }
        }
    )*)
}

// Below should work and be more correct, but it causes a strange recursive problem...
//
// impl<R, O> $t<Expr<R>> for $p
// where
//     R: ResolvesToType + Node,
//     $p: $t<R::Type, Output=O>,
//     O: RenderTypeStr + 'static
// {
//     type Output = Expr<BinaryExpr<O>>;

//     fn $m(self, right: Expr<R>) -> Self::Output {
//         Expr::new(BinaryExpr::new::<O>($e, self.into(), right.into()))
//     }
// }

// impl<R, O> $t<Expr<R>> for &$p
// where
//     R: ResolvesToType + Node,
//     $p: $t<R::Type, Output=O>,
//     O: RenderTypeStr + 'static
// {
//     type Output = Expr<BinaryExpr<O>>;

//     fn $m(self, right: Expr<R>) -> Self::Output {
//         Expr::new(BinaryExpr::new::<O>($e, self.into(), right.into()))
//     }
// }
// impl<R, O> $t<&Expr<R>> for $p
// where
//     R: ResolvesToType + Node,
//     $p: $t<R::Type, Output=O>,
//     O: RenderTypeStr + 'static
// {
//     type Output = Expr<BinaryExpr<O>>;

//     fn $m(self, right: &Expr<R>) -> Self::Output {
//         Expr::new(BinaryExpr::new::<O>($e, self.clone().into(), right.into()))
//     }
// }

// impl<R, O> $t<&Expr<R>> for &$p
// where
//     R: ResolvesToType + Node,
//     $p: $t<R::Type, Output=O>,
//     O: RenderTypeStr + 'static
// {
//     type Output = Expr<BinaryExpr<O>>;

//     fn $m(self, right: &Expr<R>) -> Self::Output {
//         Expr::new(BinaryExpr::new::<O>($e, self.clone().into(), right.into()))
//     }
// }

use std::ops::*;

ops_impl! {
    Add ; BinaryOpType::Add ; add,
    BitAnd ; BinaryOpType::BitAnd ; bitand,
    BitOr ; BinaryOpType::BitOr ; bitor,
    BitXor ; BinaryOpType::BitXor ; bitxor,
    Div ; BinaryOpType::Div ; div,
    Mul ; BinaryOpType::Mul ; mul,
    Rem ; BinaryOpType::Rem ; rem,
    Shl ; BinaryOpType::Shl ; shl,
    Shr ; BinaryOpType::Shr ; shr,
    Sub ; BinaryOpType::Sub ; sub
}

impl_binary_for_prim_on_left! { Add ; BinaryOpType::Add ; add ; i8, u8, i16, u16, i32, u32, i64, u64, f32, f64, Complex32, Complex64}
impl_binary_for_prim_on_left! { BitAnd ; BinaryOpType::BitAnd ; bitand ; i8, u8, i16, u16, i32, u32, i64, u64}
impl_binary_for_prim_on_left! { BitOr ; BinaryOpType::BitOr ; bitor ; i8, u8, i16, u16, i32, u32, i64, u64}
impl_binary_for_prim_on_left! { BitXor ; BinaryOpType::BitXor ; bitxor ; i8, u8, i16, u16, i32, u32, i64, u64}
impl_binary_for_prim_on_left! { Div ; BinaryOpType::Div ; div ; i8, u8, i16, u16, i32, u32, i64, u64, f32, f64, Complex32, Complex64}
impl_binary_for_prim_on_left! { Mul ; BinaryOpType::Mul ; mul ; i8, u8, i16, u16, i32, u32, i64, u64, f32, f64, Complex32, Complex64}
impl_binary_for_prim_on_left! { Rem ; BinaryOpType::Rem ; rem ; i8, u8, i16, u16, i32, u32, i64, u64, f32, f64, Complex32, Complex64}
impl_binary_for_prim_on_left! { Shl ; BinaryOpType::Shl ; shl ; i8, u8, i16, u16, i32, u32, i64, u64}
impl_binary_for_prim_on_left! { Shr ; BinaryOpType::Shr ; shr ; i8, u8, i16, u16, i32, u32, i64, u64}
impl_binary_for_prim_on_left! { Sub ; BinaryOpType::Sub ; sub ; i8, u8, i16, u16, i32, u32, i64, u64, f32, f64, Complex32, Complex64}
