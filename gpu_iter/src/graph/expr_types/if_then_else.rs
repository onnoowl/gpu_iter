use super::*;

use std::collections::HashMap;
use std::fmt;
use std::marker::PhantomData;

#[derive(Debug)]
pub struct IfThenElse<T: RenderTypeStr> {
    cond: DynNode,
    then: DynNode,
    els: DynNode,
    _pht: PhantomData<T>,
}

pub fn if_then_els<Cond, Then, Els>(cond: Cond, then: Then, els: Els) -> Expr<Then::Type>
where
    Cond: IntoExpr<Type = bool>,
    Then: IntoExpr,
    Els: IntoExpr<Type = Then::Type>,
{
    Expr::new(IfThenElse::new(
        cond.into_expr().node,
        then.into_expr().node,
        els.into_expr().node,
    ))
}

pub fn if_then<Cond, Then>(cond: Cond, then: Then) -> IfBuilderNeedsElse<Then::Type>
where
    Cond: IntoExpr<Type = bool>,
    Then: IntoExpr,
{
    IfBuilderNeedsElse::new(cond.into_expr().node, then.into_expr().node)
}

impl<T: RenderTypeStr> IfThenElse<T> {
    pub(crate) fn new(cond: DynNode, then: DynNode, els: DynNode) -> Self {
        IfThenElse {
            cond,
            then,
            els,
            _pht: PhantomData,
        }
    }
}

impl<T: RenderTypeStr> Node for IfThenElse<T> {
    fn call_on_children(&self, fun: &mut dyn FnMut(&[DynNode])) {
        fun(&[self.cond.clone(), self.then.clone(), self.els.clone()])
    }

    fn render_with(
        &self,
        f: &mut fmt::Formatter<'_>,
        self_key: u64,
        meta_map: &HashMap<u64, NodeMetaData>,
    ) -> fmt::Result {
        let var_num = meta_map
            // .get(&DynNode::with_arc((self.clone() as Arc<dyn Node>)).default_ptr_hash())
            .get(&self_key)
            .expect("If statement failed to get a meta entry created during graph traversal.")
            .var_num
            .expect("If statement failed to have var_num assigned by graph traversal.");
        write!(
            f,
            "if({cond}) {{\n    {prefix}_var{var_num} = {then};\n}} else {{\n    {prefix}_var{var_num} = {els};\n}}",
            cond=self.cond.clone().renderer(meta_map),
            prefix = VAR_PREFIX,
            var_num = var_num,
            then=self.then.clone().renderer(meta_map),
            els=self.els.clone().renderer(meta_map)
        )
    }

    fn is_statement(&self) -> bool {
        true
    }
}

impl<T: RenderTypeStr> ResolvesToType for IfThenElse<T> {
    type Type = T;
}

pub struct IfBuilderNeedsElse<T: RenderTypeStr> {
    cond: DynNode,
    then: DynNode,
    _pht: PhantomData<T>,
}

impl<T: RenderTypeStr> IfBuilderNeedsElse<T> {
    pub(crate) fn new(cond: DynNode, then: DynNode) -> IfBuilderNeedsElse<T> {
        IfBuilderNeedsElse {
            cond,
            then,
            _pht: PhantomData,
        }
    }

    pub fn els<Else>(self, els: Else) -> Expr<T>
    where
        Else: IntoExpr<Type = T>,
    {
        Expr::new(IfThenElse::new(self.cond, self.then, els.into_expr().node))
    }
}
