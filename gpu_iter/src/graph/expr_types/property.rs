use super::*;

use std::collections::HashMap;
use std::fmt;
use std::marker::PhantomData;

#[derive(Debug)]
pub struct PropertyAccess<T: RenderTypeStr> {
    inner: DynNode,
    property: &'static str,
    _pht: PhantomData<T>,
}

impl<T: RenderTypeStr> PropertyAccess<T> {
    pub fn new(inner: DynNode, property: &'static str) -> Self {
        PropertyAccess {
            inner,
            property,
            _pht: PhantomData,
        }
    }
}

impl<T: RenderTypeStr> Node for PropertyAccess<T> {
    fn call_on_children(&self, fun: &mut dyn FnMut(&[DynNode])) {
        fun(&[self.inner.clone()])
    }

    fn render_with(
        &self,
        f: &mut fmt::Formatter<'_>,
        _self_key: u64,
        meta_map: &HashMap<u64, NodeMetaData>,
    ) -> fmt::Result {
        write!(
            f,
            "({}).{}",
            self.inner.clone().renderer(meta_map),
            self.property
        )
    }
}

impl<T: RenderTypeStr> ResolvesToType for PropertyAccess<T> {
    type Type = T;
}
