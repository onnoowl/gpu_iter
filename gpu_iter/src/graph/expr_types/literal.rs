use super::*;

use std::collections::HashMap;
use std::fmt;
use std::marker::PhantomData;

use num_complex::{Complex32, Complex64};
use ocl::prm::*;

pub trait WriteLiteralStr: RenderTypeStr + fmt::Debug {
    fn write_literal_str(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result;
}

#[derive(Debug)]
pub struct Literal<T: WriteLiteralStr> {
    data: T,
    _pht: PhantomData<T>,
}

impl<T: WriteLiteralStr> Literal<T> {
    pub fn new(data: T) -> Self {
        Literal {
            data,
            _pht: PhantomData,
        }
    }
}

impl<T: WriteLiteralStr> From<T> for Literal<T> {
    fn from(l: T) -> Self {
        Literal::new(l)
    }
}

impl<T: WriteLiteralStr> IntoExpr for T {
    type Type = T;

    fn into_expr(self) -> Expr<Self::Type> {
        Expr::new(Literal::new(self))
    }
}
impl<T: WriteLiteralStr> NonReflexiveIntoExpr for T {}

impl<T: WriteLiteralStr + 'static> Node for Literal<T> {
    fn call_on_children(&self, fun: &mut dyn FnMut(&[DynNode])) {
        fun(&[])
    }

    fn render_with(
        &self,
        f: &mut fmt::Formatter<'_>,
        _self_key: u64,
        _meta_map: &HashMap<u64, NodeMetaData>,
    ) -> fmt::Result {
        write!(f, "({})", T::render_type())?;
        self.data.write_literal_str(f)
    }
}

macro_rules! write_literal_str {
    ($($t:ty),*) => ($(
        impl WriteLiteralStr for $t {
            fn write_literal_str(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                use std::fmt::Display;
                self.fmt(f)
            }
        }
    )*)
}

write_literal_str! {
    bool, i8, u8, i16, u16, i32, u32, i64, u64, f32, f64,
    Complex32, Complex64,
    Char, Uchar, Short, Ushort, Int, Uint, Long, Ulong, Float, Double
}

impl<T: WriteLiteralStr> ResolvesToType for Literal<T> {
    type Type = T;
}
