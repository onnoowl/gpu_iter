use super::*;

use std::collections::HashMap;
use std::fmt;
use std::marker::PhantomData;

use smallvec::SmallVec;

pub type Args = SmallVec<[DynNode; 2]>;

#[derive(Debug)]
pub struct Function<T: RenderTypeStr> {
    name: &'static str,
    args: Args,
    _pht: PhantomData<T>,
}

impl<T: RenderTypeStr> Function<T> {
    pub fn new(name: &'static str, args: Args) -> Self {
        Function {
            name,
            args,
            _pht: PhantomData,
        }
    }
}

impl<T: RenderTypeStr> Node for Function<T> {
    fn call_on_children(&self, fun: &mut dyn FnMut(&[DynNode])) {
        fun(&self.args[..])
    }
    fn render_with(
        &self,
        f: &mut fmt::Formatter<'_>,
        _self_key: u64,
        meta_map: &HashMap<u64, NodeMetaData>,
    ) -> fmt::Result {
        write!(f, "{}(", self.name)?;
        for (i, arg) in self.args.iter().enumerate() {
            write!(f, "{}", arg.clone().renderer(meta_map))?;
            if i < self.args.len() - 1 {
                write!(f, ", ")?;
            }
        }
        write!(f, ")")
    }
}

impl<T: RenderTypeStr> ResolvesToType for Function<T> {
    type Type = T;
}
