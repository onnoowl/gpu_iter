use super::*;

use std::collections::HashMap;
use std::fmt;
use std::marker::PhantomData;

#[derive(Debug)]
pub struct InputVar<T: RenderTypeStr> {
    var_num: u64,
    _pht: PhantomData<T>,
}

impl<T: RenderTypeStr> InputVar<T> {
    pub fn new(var_num: u64) -> Self {
        InputVar {
            var_num,
            _pht: PhantomData,
        }
    }
}

impl<T: RenderTypeStr> Node for InputVar<T> {
    fn call_on_children(&self, fun: &mut dyn FnMut(&[DynNode])) {
        fun(&[])
    }
    fn render_with(
        &self,
        f: &mut fmt::Formatter<'_>,
        _self_key: u64,
        _meta_map: &HashMap<u64, NodeMetaData>,
    ) -> fmt::Result {
        write!(f, "var{}", self.var_num)
    }
}

impl<T: RenderTypeStr> ResolvesToType for InputVar<T> {
    type Type = T;
}
