use super::*;

#[allow(unused_imports)] //Needed for macro expansions
use crate as gpu_iter;
use crate::to_gpu::ToGpu;

use std::collections::HashMap;
use std::fmt;
use std::marker::PhantomData;

use ocl::builders::KernelBuilder;
use ocl::prm::*;

use arrayvec::ArrayVec;

// Begin Vec Traits

pub trait VecTrait: RenderTypeStr {
    type VInnerType: RenderTypeStr;
}

pub trait VecAtleast2: VecTrait {}
pub trait VecAtleast3: VecTrait {}
pub trait VecAtleast4: VecTrait {}
pub trait VecAtleast8: VecTrait {}
pub trait VecAtleast16: VecTrait {}

// End Vec Traits

// Begin VecAtleast Traits impls

macro_rules! vec_impl_atleasts_for_ocl {
    ($t:ty : $n:tt : ($($atleasts:literal),*)) => ($(
        paste::item! {
            impl [<VecAtleast $atleasts>] for [<$t $n>] {}
        }
    )*)
}

macro_rules! vec_impl_atleasts_for_vec_expr {
    ($t:ty : $n:tt : ($($atleasts:literal),*)) => ($(
        vec_impl_atleasts_for_vec_expr_for_v_inner_type! {
            $t : $n : $atleasts ; i8, u8, i16, u16, i32, u32, i64, u64, f32, f64
        }
    )*)
}

macro_rules! vec_impl_atleasts_for_vec_expr_for_v_inner_type {
    ($t:ty : $n:tt : $atleasts:literal ; $($inner_type:ty),*) => ($(
        paste::item! {
            impl [<VecAtleast $atleasts>] for [<$t $n>]<$inner_type> {}
        }
    )*)
}

impl<V: VecAtleast2> Expr<V> {
    pub fn x(&self) -> Expr<V::VInnerType> {
        Expr::new(PropertyAccess::new(self.node.clone(), "x"))
    }

    pub fn y(&self) -> Expr<V::VInnerType> {
        Expr::new(PropertyAccess::new(self.node.clone(), "y"))
    }

    pub fn s0(&self) -> Expr<V::VInnerType> {
        Expr::new(PropertyAccess::new(self.node.clone(), "s0"))
    }

    pub fn s1(&self) -> Expr<V::VInnerType> {
        Expr::new(PropertyAccess::new(self.node.clone(), "s1"))
    }
}

impl<V: VecAtleast3> Expr<V> {
    pub fn z(&self) -> Expr<V::VInnerType> {
        Expr::new(PropertyAccess::new(self.node.clone(), "z"))
    }

    pub fn s2(&self) -> Expr<V::VInnerType> {
        Expr::new(PropertyAccess::new(self.node.clone(), "s2"))
    }
}

impl<V: VecAtleast4> Expr<V> {
    pub fn w(&self) -> Expr<V::VInnerType> {
        Expr::new(PropertyAccess::new(self.node.clone(), "w"))
    }

    pub fn s3(&self) -> Expr<V::VInnerType> {
        Expr::new(PropertyAccess::new(self.node.clone(), "s3"))
    }
}

macro_rules! s_access {
    ($t:tt : $($s:tt),*) => (
        impl<V: $t> Expr<V> {
            $(
                #[allow(non_snake_case)]
                pub fn $s(&self) -> Expr<V::VInnerType> {
                    Expr::new(PropertyAccess::new(self.node.clone(), stringify!($s)))
                }
            )*
        }
    )
}

s_access! { VecAtleast8 : s4, s5, s6, s7 }
s_access! { VecAtleast16 : s8, s9, sA, sB, sC, sD, sE, sF }

// End VecAtleast Traits impls

// Begin VecExpr
#[macro_export]
macro_rules! vector {
    ($x:expr, $y:expr) => {
        gpu_iter::Expr::new(gpu_iter::graph::VecExpr2::new([
            $x.into_expr(),
            $y.into_expr(),
        ]))
    };
    ($x:expr, $y:expr, $z:expr) => {
        gpu_iter::Expr::new(gpu_iter::graph::VecExpr3::new([
            $x.into_expr(),
            $y.into_expr(),
            $z.into_expr(),
        ]))
    };
    ($x:expr, $y:expr, $z:expr, $w:expr) => {
        gpu_iter::Expr::new(gpu_iter::graph::VecExpr4::new([
            $x.into_expr(),
            $y.into_expr(),
            $z.into_expr(),
            $w.into_expr(),
        ]))
    };
    ($s0:expr, $s1:expr, $s2:expr, $s3:expr, $s4:expr, $s5:expr, $s6:expr, $s7:expr) => {
        gpu_iter::Expr::new(gpu_iter::graph::VecExpr8::new([
            $s0.into_expr(),
            $s1.into_expr(),
            $s2.into_expr(),
            $s3.into_expr(),
            $s4.into_expr(),
            $s5.into_expr(),
            $s6.into_expr(),
            $s7.into_expr(),
        ]))
    };
    (
        $s0:expr, $s1:expr, $s2:expr, $s3:expr, $s4:expr, $s5:expr, $s6:expr, $s7:expr,
        $s8:expr, $s9:expr, $sA:expr, $sB:expr, $sC:expr, $sD:expr, $sE:expr, $sF:expr
    ) => {
        gpu_iter::Expr::new(gpu_iter::graph::VecExpr16::new([
            $s0.into_expr(),
            $s1.into_expr(),
            $s2.into_expr(),
            $s3.into_expr(),
            $s4.into_expr(),
            $s5.into_expr(),
            $s6.into_expr(),
            $s7.into_expr(),
            $s8.into_expr(),
            $s9.into_expr(),
            $sA.into_expr(),
            $sB.into_expr(),
            $sC.into_expr(),
            $sD.into_expr(),
            $sE.into_expr(),
            $sF.into_expr(),
        ]))
    };
}

macro_rules! vec_node_impl_for_prim {
    ($n:tt ; $($p:ty),*) => ($(
        paste::item! {
            impl Node for [<VecExpr $n>]<$p> {
                fn call_on_children(&self, fun: &mut dyn FnMut(&[DynNode])) {
                    fun(&self.items[..])
                }
                fn render_with(
                    &self,
                    f: &mut fmt::Formatter<'_>,
                    _self_key: u64,
                    meta_map: &HashMap<u64, NodeMetaData>,
                ) -> fmt::Result {
                    write!(f, "({})(", self.render_type())?;

                    self.items.iter().take($n - 1).cloned().map(|x| write!(f, "{}, ", x.renderer(meta_map))).collect::<fmt::Result>()?;
                    write!(f, "{})", self.items.last().unwrap().clone().renderer(meta_map))
                }
            }
        }
    )*)
}

macro_rules! vec_expr_impl {
    ($($n:expr),*) => ($(
        paste::item! {
            #[derive(Debug)]
            pub struct [<VecExpr $n>]<T: RenderTypeStr> {
                pub(crate) items: [DynNode ; $n],
                _pht: PhantomData<T>
            }

            impl<T: RenderTypeStr> [<VecExpr $n>]<T> {
                pub fn new(items: [Expr<T>; $n]) -> Self {
                    [<VecExpr $n>] {
                        items: items
                            .iter()
                            .map(|i| i.node.clone())
                            .collect::<ArrayVec<_>>()
                            .into_inner()
                            .unwrap(),
                        _pht: PhantomData
                    }
                }
            }
        }

        vec_node_impl_for_prim!{ $n ; i8, u8, i16, u16, i32, u32, i64, u64, f32, f64 }
    )*)
}
vec_expr_impl! { 2, 3, 4, 8, 16 }
vec_impl_atleasts_for_vec_expr! { VecExpr : 2 : (2) }
vec_impl_atleasts_for_vec_expr! { VecExpr : 3 : (2, 3) }
vec_impl_atleasts_for_vec_expr! { VecExpr : 4 : (2, 3, 4) }
vec_impl_atleasts_for_vec_expr! { VecExpr : 8 : (2, 3, 4, 8) }
vec_impl_atleasts_for_vec_expr! { VecExpr : 16 : (2, 3, 4, 8, 16) }

// End VecExpr

// Begin impls for OCL vec types

macro_rules! ocl_vec_impl {
    ($t:ty : $p:ty : $s:expr ; $($n:tt : $atleasts:expr),*) => ($(
        paste::item! {
            impl RenderTypeStr for [<$t $n>] {
                fn impl_render(f: &mut fmt::Formatter<'_>) -> fmt::Result {
                    write!(f, concat!($s, stringify!($n)))
                }
                fn impl_render_struct_defn(_f: &mut fmt::Formatter<'_>) -> fmt::Result {
                    Ok(())
                }
                fn needs_struct_defn() -> bool {
                    false
                }
            }

            impl ResolvesToType for [<VecExpr $n>]<$p> {
                type Type = [<$t $n>];
            }

            impl VecTrait for [<VecExpr $n>]<$p> {
                type VInnerType = $p;
            }

            impl VecTrait for [<$t $n>] {
                type VInnerType = $p;
            }

            vec_impl_atleasts_for_ocl! { $t : $n : $atleasts }

            impl ToGpu for [<$t $n>] {
                fn add_to_kernel_builder<'s: 'b, 'k, 'b>(
                    &'s self,
                    kb: &'k mut KernelBuilder<'b>,
                ) -> &'k mut KernelBuilder<'b> {
                    kb.arg(self)
                }
            }

            impl WriteLiteralStr for [<$t $n>] {
                fn write_literal_str(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                    write!(f, "(")?;
                    self.iter().take($n - 1).map(|x| write!(f, "{}, ", x)).collect::<fmt::Result>()?;
                    write!(f, "{})", self.get($n - 1).expect("VecAtleasttor did not have expected length"))
                }
            }
        }
    )*)
}

macro_rules! ocl_vec_impl_for_type {
    ($($t:ty : $p:ty : $s:expr),*) => ($(
        ocl_vec_impl! { $t : $p : $s ; 2 : (2), 3 : (2, 3), 4 : (2, 3, 4), 8 : (2, 3, 4, 8), 16 : (2, 3, 4, 8, 16) }
    )*)
}

ocl_vec_impl_for_type! {
   Char:i8:"char", Uchar:u8:"uchar", Short:i16:"short", Ushort:u16:"ushort", Int:i32:"int", Uint:u32:"uint", Long:i64:"long", Ulong:u64:"ulong", Float:f32:"float", Double:f64:"double"
}

// End impls for OCL vec types

#[cfg(test)]
mod tests {
    use super::*;
    use crate::gpu_iter::IntoGpuIter;

    #[test]
    fn vec_expr() {
        let v = vector!(Expr::from(1) + 2, 3, 4);

        assert_eq!(
            format!("{}", v),
            "int3 __gpu_iter_result = (int3)(((int)1 + (int)2), (int)3, (int)4);\n"
        );

        let _ = vector!(1, 2);
        let _ = vector!(1, 2, 3);
        let _ = vector!(1, 2, 3, 4);
        let _ = vector!(1, 2, 3, 4, 5, 6, 7, 8);
        let _ = vector!(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
    }

    #[test]
    fn vector_test() {
        let e: Expr<_> = Int3::new(1, 2, 3).into();
        let e2: Expr<_> = Int8::new(1, 2, 3, 4, 5, 6, 7, 8).into_expr().s5();
        let out = e.x() + e.y() + e.z();

        assert_eq!(
            format!("{}", e),
            "int3 __gpu_iter_result = (int3)(1, 2, 3);\n"
        );

        assert_eq!(
            format!("{}", e2),
            "int __gpu_iter_result = ((int8)(1, 2, 3, 4, 5, 6, 7, 8)).s5;\n"
        );

        assert_eq!(
            format!("{}", out),
            "int3 __gpu_iter_var0 = (int3)(1, 2, 3);\nint __gpu_iter_result = (((__gpu_iter_var0).x + (__gpu_iter_var0).y) + (__gpu_iter_var0).z);\n"
        );
    }

    #[test]
    fn vec_in_and_out() -> Result<(), crate::error::Error> {
        let out: Vec<Int3> = vec![Int3::new(1, 2, 3), Int3::new(4, 5, 6)]
            .gpu_iter()
            .map(|v| v + Int3::new(1, 1, 1))
            .collect()?;

        assert_eq!(out[..], [Int3::new(2, 3, 4), Int3::new(5, 6, 7)]);

        Ok(())
    }
}
