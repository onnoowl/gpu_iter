use crate::get_type_str::{DynRenderTypeStr, RenderTypeStr};
use crate::graph::ResolvesToType;

use std::fmt;
use std::marker::PhantomData;

use ocl::builders::KernelBuilder;
use ocl::prm::*;

pub trait ToGpu: DynRenderTypeStr {
    fn add_to_kernel_builder<'s: 'b, 'k, 'b>(
        &'s self,
        kb: &'k mut KernelBuilder<'b>,
    ) -> &'k mut KernelBuilder<'b>;
}

// For structs that need a "GPUType" to be used within lambda expressions so that the graph logic works
pub trait HasGPUType: RenderTypeStr + Sized {
    type GPUType: ResolvesToType<Type = Self>;

    fn render_struct_defn() -> StructDefnRenderer<Self>
    where
        Self: Sized,
    {
        StructDefnRenderer(PhantomData)
    }
    fn impl_render_struct_defn(f: &mut fmt::Formatter<'_>) -> fmt::Result;
}

pub struct StructDefnRenderer<T: RenderTypeStr>(PhantomData<T>);
impl<T: HasGPUType> fmt::Display for StructDefnRenderer<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> fmt::Result {
        <T as HasGPUType>::impl_render_struct_defn(f)
    }
}

macro_rules! impl_to_gpu {
    ($($t:ty),*) => ($(
        impl ToGpu for $t {
            fn add_to_kernel_builder<'s: 'b, 'k, 'b>(
                &'s self,
                kb: &'k mut KernelBuilder<'b>,
            ) -> &'k mut KernelBuilder<'b> {
                kb.arg(self)
            }
        }
    )*)
}

impl_to_gpu! {
    i8, u8, i16, u16, i32, u32, i64, u64, f32, f64,
    Char, Uchar, Short, Ushort, Int, Uint, Long, Ulong, Float, Double
}
