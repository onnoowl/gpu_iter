use super::*;

use ocl::{builders::KernelBuilder, traits::OclPrm, Buffer, Context};

use std::any::{Any, TypeId};
use std::collections::hash_map::DefaultHasher;

#[derive(Debug)]
//SAB (Slice and Buffer) stores a pointer to a slice (on the host), but also stores a corresponding GPU buffer.
pub struct SAB<'s, T: ToGpu + RenderTypeStr + OclPrm + Any> {
    pub slice: SliceType<'s, T>,
    pub opt_buffer: Option<Buffer<T>>,
}

impl<'s, T: ToGpu + RenderTypeStr + OclPrm + Any> SAB<'s, T> {
    pub fn new(slice: SliceType<'s, T>) -> Self {
        SAB {
            slice,
            opt_buffer: None,
        }
    }
}

pub trait HasOptBuffer {
    fn create_buffer(&mut self, context: &Context) -> ocl::Result<()>;
    fn get_buffer_as_any(&self) -> &dyn Any;
}

fn build_buffer<T: OclPrm>(context: &Context, slice: &[T]) -> ocl::Result<Buffer<T>> {
    Buffer::builder()
        .copy_host_slice(slice)
        .len(slice.len())
        .context(context)
        .build()
}

impl<'s, T> DynRenderTypeStr for SAB<'s, T>
where
    T: ToGpu + RenderTypeStr + OclPrm + Any,
{
    fn impl_render(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}*", <T as RenderTypeStr>::render_type())
    }
    fn impl_render_struct_defn(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        <T as RenderTypeStr>::impl_render_struct_defn(f)
    }
    fn needs_struct_defn(&self) -> bool {
        <T as RenderTypeStr>::needs_struct_defn()
    }
}

impl<T> HasOptBuffer for SAB<'_, T>
where
    T: ToGpu + RenderTypeStr + OclPrm + Any,
{
    fn create_buffer(&mut self, context: &Context) -> ocl::Result<()> {
        if self.opt_buffer.is_none() {
            self.opt_buffer = Some(build_buffer(context, self.slice.as_immut())?);
        }
        Ok(())
    }

    /// Panics if called before `create_buffer`.
    fn get_buffer_as_any(&self) -> &dyn Any {
        self.opt_buffer
            .as_ref()
            .expect("Must be called after `HasOptBuffer::create_buffer`")
    }
}

impl<T> ToGpu for SAB<'_, T>
where
    T: ToGpu + RenderTypeStr + OclPrm + Any,
{
    /// Panics if SAB's create_buffer hasn't been called yet.
    fn add_to_kernel_builder<'s: 'b, 'k, 'b>(
        &'s self,
        kb: &'k mut KernelBuilder<'b>,
    ) -> &'k mut KernelBuilder<'b> {
        kb.arg(
            self.opt_buffer
                .as_ref()
                .expect("Buffer not present. Ensure SAB::create_buffer is called before this."),
        )
    }
}

impl<'s, T> IsImmutOrMut for SAB<'s, T>
where
    T: ToGpu + RenderTypeStr + OclPrm + Any,
{
    fn is_mut(&self) -> bool {
        self.slice.is_mut()
    }
}

impl<'s, T> DefaultHash for SAB<'s, T>
where
    T: ToGpu + RenderTypeStr + OclPrm + Any,
{
    fn hash(&self, state: &mut DefaultHasher) {
        TypeId::of::<T>().hash(state);
        self.slice.hash(state);
    }
}

impl<'s, T> RefType for SAB<'s, T>
where
    T: ToGpu + RenderTypeStr + OclPrm + Any,
{
    fn deref_type_id(&self) -> TypeId {
        TypeId::of::<T>()
    }

    fn deref_impl_render(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        <T as RenderTypeStr>::impl_render(f)
    }
}

pub trait SABTrait<'s>: HasOptBuffer + ToGpu + IsImmutOrMut + DefaultHash + RefType {}

impl<'s: 'd, 'd, T: ToGpu + RenderTypeStr + OclPrm + Any> SABTrait<'s> for SAB<'s, T> {}

impl<'s: 'd, 'd, T> ImmutSliceToSrcType<'s, 'd> for &'s [T]
where
    T: ToGpu + RenderTypeStr + OclPrm + Any,
{
    type SliceRefType = T;

    fn to_src_type(self) -> SrcType<'s, 'd> {
        SrcType::SAB(Box::new(SAB::new(SliceType::Slice(self))))
    }
    fn len(&self) -> usize {
        <[T]>::len(self)
    }
    fn is_empty(&self) -> bool {
        <[T]>::is_empty(self)
    }
}

impl<'s: 'd, 'd, T> MutSliceToSrcType<'s, 'd> for &'s mut [T]
where
    T: ToGpu + RenderTypeStr + OclPrm + Any,
{
    type SliceRefType = T;

    fn to_src_type(self) -> SrcType<'s, 'd> {
        SrcType::SAB(Box::new(SAB::new(SliceType::SliceMut(self))))
    }
    fn len(&self) -> usize {
        <[T]>::len(self)
    }
    fn is_empty(&self) -> bool {
        <[T]>::is_empty(self)
    }
}

#[derive(Debug)]
pub enum SliceType<'s, T: ToGpu + DynRenderTypeStr + 'static> {
    Slice(&'s [T]),
    SliceMut(&'s mut [T]),
}

impl<'s, T: ToGpu + DynRenderTypeStr + 'static> SliceType<'s, T> {
    fn as_immut<'d: 's>(&'d self) -> &'s [T] {
        match self {
            SliceType::Slice(inner) => &*inner,
            SliceType::SliceMut(inner) => &*inner,
        }
    }
}

impl<'s, T: ToGpu + DynRenderTypeStr + 'static> IsImmutOrMut for SliceType<'s, T> {
    fn is_mut(&self) -> bool {
        use SliceType::*;
        match self {
            Slice(_) => false,
            SliceMut(_) => true,
        }
    }
}

impl<'s, T: ToGpu + DynRenderTypeStr + 'static> PartialEq for SliceType<'s, T> {
    fn eq(&self, other: &Self) -> bool {
        use SliceType::*;
        match (self, other) {
            (Slice(_), Slice(_)) => true,
            (SliceMut(_), SliceMut(_)) => true,
            (_, _) => false,
        }
    }
}

#[derive(Hash)]
enum SliceTypeVariants {
    Slice,
    SliceMut,
}

impl<'s, T: ToGpu + DynRenderTypeStr + 'static> Hash for SliceType<'s, T> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        TypeId::of::<T>().hash(state);

        use SliceType::*;
        match self {
            Slice(_) => SliceTypeVariants::Slice.hash(state), //just need to be unique compared to each other.
            SliceMut(_) => SliceTypeVariants::SliceMut.hash(state),
        }
    }
}

pub trait ImmutSliceToSrcType<'s, 'd> {
    type SliceRefType: DynRenderTypeStr + OclPrm + Any;

    fn to_src_type(self) -> SrcType<'s, 'd>;
    fn len(&self) -> usize;
    fn is_empty(&self) -> bool;
}

pub trait MutSliceToSrcType<'s, 'd> {
    type SliceRefType: DynRenderTypeStr + OclPrm + Any;

    fn to_src_type(self) -> SrcType<'s, 'd>;
    fn len(&self) -> usize;
    fn is_empty(&self) -> bool;
}
