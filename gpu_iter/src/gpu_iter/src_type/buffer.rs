use super::*;

use ocl::{builders::KernelBuilder, traits::OclPrm, Buffer};

use std::any::TypeId;

pub trait BufferTrait: ToGpu + RefType {}

impl<T: ToGpu + RenderTypeStr + OclPrm> RefType for Buffer<T> {
    fn deref_type_id(&self) -> TypeId {
        TypeId::of::<T>()
    }
    fn deref_impl_render(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        <T as RenderTypeStr>::impl_render(f)
    }
}

impl<T: ToGpu + RenderTypeStr + OclPrm> BufferTrait for Buffer<T> {}

impl<T: ToGpu + RenderTypeStr + OclPrm> DynRenderTypeStr for Buffer<T> {
    fn impl_render(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}*", self.deref_render_type())
    }
    fn impl_render_struct_defn(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        <T as RenderTypeStr>::impl_render_struct_defn(f)
    }
    fn needs_struct_defn(&self) -> bool {
        <T as RenderTypeStr>::needs_struct_defn()
    }
}

impl<T: ToGpu + RenderTypeStr + OclPrm> ToGpu for Buffer<T> {
    fn add_to_kernel_builder<'a: 'b, 's, 'b>(
        &'a self,
        kb: &'s mut KernelBuilder<'b>,
    ) -> &'s mut KernelBuilder<'b> {
        kb.arg(self)
    }
}
