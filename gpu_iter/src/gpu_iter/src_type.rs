mod buffer;
mod sab;

pub use buffer::*;
pub use sab::*;

use crate::get_type_str::{DynRenderTypeStr, RenderTypeStr};
use crate::to_gpu::ToGpu;

use std::any::TypeId;
use std::collections::hash_map::DefaultHasher;
use std::fmt;
use std::hash::{Hash, Hasher};
use std::sync::atomic::{AtomicU64, Ordering};

static INPUT_VAR_COUNTER: AtomicU64 = AtomicU64::new(0);

fn next_var_num() -> u64 {
    INPUT_VAR_COUNTER.fetch_add(1, Ordering::Relaxed)
}

pub struct Src<'s: 'd, 'd> {
    pub(crate) var_num: u64,
    pub(crate) src_type: SrcType<'s, 'd>,
}

impl<'s: 'd, 'd> Src<'s, 'd> {
    pub fn new(src_type: SrcType<'s, 'd>) -> Self {
        Src {
            var_num: next_var_num(),
            src_type,
        }
    }
}

// pub trait BufferRead {
//     fn read_from_buffer<T: OclPrm + Any>(&self, dst: &mut [T], queue: &Queue) -> ocl::Result<()>;
// }

// pub trait InputBuffer: BufferRead + ToGpu {}

pub trait IsImmutOrMut {
    fn is_mut(&self) -> bool;
    fn is_immut(&self) -> bool {
        !self.is_mut()
    }
}

pub trait DefaultHash {
    fn hash(&self, state: &mut DefaultHasher);
}

pub enum SrcType<'s: 'd, 'd> {
    SAB(Box<dyn SABTrait<'s> + 'd>),
    Buffer(&'s dyn BufferTrait),
    MutBuffer(&'s mut dyn BufferTrait),
    WithArg(&'s dyn ToGpu),
}

impl<'s: 'd, 'd> DynRenderTypeStr for SrcType<'s, 'd> {
    fn impl_render(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use SrcType::*;
        match self {
            SAB(x) => x.impl_render(f),
            Buffer(x) => x.impl_render(f),
            MutBuffer(x) => x.impl_render(f),
            WithArg(x) => x.impl_render(f),
        }
    }
    fn impl_render_struct_defn(&self, _f: &mut fmt::Formatter<'_>) -> fmt::Result {
        Ok(())
    }
    fn needs_struct_defn(&self) -> bool {
        false
    }
}

impl<'s: 'd, 'd> IsImmutOrMut for SrcType<'s, 'd> {
    fn is_mut(&self) -> bool {
        use SrcType::*;
        match self {
            SAB(x) => x.is_mut(),
            Buffer(_) => false,
            MutBuffer(_) => true,
            WithArg(_) => false,
        }
    }
}

// These variants are the same as SrcType, but with no inner contents.
// This is used to generated a Hash impl for the variants.
#[derive(Hash)]
enum SrcTypeVariants {
    SAB,
    Buffer,
    MutBuffer,
    WithArg,
}

impl<'s: 'd, 'd> DefaultHash for SrcType<'s, 'd> {
    fn hash(&self, state: &mut DefaultHasher) {
        use SrcType::*;

        match self {
            SAB(x) => {
                SrcTypeVariants::SAB.hash(state);
                x.hash(state);
            }
            Buffer(_) => SrcTypeVariants::Buffer.hash(state),
            MutBuffer(_) => SrcTypeVariants::MutBuffer.hash(state),
            WithArg(_) => SrcTypeVariants::WithArg.hash(state),
        }
    }
}

impl<'s: 'd, 'd> DefaultHash for Src<'s, 'd> {
    fn hash(&self, state: &mut DefaultHasher) {
        self.src_type.hash(state);
    }
}

pub trait RefType {
    fn deref_type_id(&self) -> TypeId;
    fn deref_impl_render(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result;

    fn deref_render_type(&self) -> DynRefTypeRenderer<'_>
    where
        Self: std::marker::Sized, //TODO: Why does this need Sized?
    {
        DynRefTypeRenderer(self)
    }
}

pub struct DynRefTypeRenderer<'a>(&'a dyn RefType);

impl<'a> fmt::Display for DynRefTypeRenderer<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> fmt::Result {
        self.0.deref_impl_render(f)
    }
}
