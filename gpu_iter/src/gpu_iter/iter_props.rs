#[derive(Debug, Hash, PartialEq, Default)]
pub struct DiscoveredProperties {
    uses_indexing: bool,
}

#[derive(Debug, Hash, PartialEq)]
pub struct DerivedProperties {
    may_mutate_source: bool, //if true, output reuses first input buffer. Otherwise, output buffer is needed
}

impl DiscoveredProperties {
    pub(crate) fn merge(&self, other: DiscoveredProperties) -> Self {
        DiscoveredProperties {
            uses_indexing: self.uses_indexing || other.uses_indexing,
        }
    }
}

impl From<DiscoveredProperties> for DerivedProperties {
    fn from(p: DiscoveredProperties) -> Self {
        DerivedProperties {
            may_mutate_source: !p.uses_indexing,
        }
    }
}
