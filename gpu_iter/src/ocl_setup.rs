use lazy_static::lazy_static;
use ocl::{
    enums::{DeviceInfo, DeviceInfoResult},
    Context, Device, Platform, Queue,
};

use std::sync::RwLock;

use crate::error::DeviceLookupError;

lazy_static! {
    static ref DEFAULT_CONT_QUEUE: RwLock<Option<ContextQueue>> =
         RwLock::new(ContextQueue::with_best_device().ok()) ;
}

pub fn default_cont_queue() -> Option<ContextQueue> {
    DEFAULT_CONT_QUEUE.read().unwrap().clone()
}

pub fn set_cont_queue(c: ContextQueue) {
    let mut writer = DEFAULT_CONT_QUEUE.write().unwrap();
    *writer = Some(c);
}

#[derive(Clone, Debug)]
pub struct ContextQueue {
    pub context: Context,
    pub queue: Queue,
}

fn max_compute_units(d: Device) -> Option<u32> {
    d.info(DeviceInfo::MaxComputeUnits).ok().and_then(|mcu| {
        if let DeviceInfoResult::MaxComputeUnits(mcu) = mcu {
            Some(mcu)
        } else {
            None
        }
    })
}

fn max_clock_frequency(d: Device) -> Option<u32> {
    d.info(DeviceInfo::MaxClockFrequency).ok().and_then(|mcu| {
        if let DeviceInfoResult::MaxClockFrequency(mcu) = mcu {
            Some(mcu)
        } else {
            None
        }
    })
}

impl ContextQueue {
    pub fn with_system_default_device() -> Result<Self, DeviceLookupError> {
        Self::with_device(Self::system_default_device)
    }

    // Attempts to find the best device with the metric MaxComputeUnits * MaxClockFrequency.
    // If no device is found, uses the system_default_device instead.
    pub fn with_best_device() -> Result<Self, DeviceLookupError> {
        Self::with_device(|p| Self::best_device(p).or_else(|_err| Self::system_default_device(p)))
    }

    pub fn with_device<F: FnOnce(Platform) -> Result<Device, DeviceLookupError>>(
        f: F,
    ) -> Result<Self, DeviceLookupError> {
        let context = Context::builder().build()?;
        let device = f(context
            .platform()?
            .expect("Default context didn't have the platform."))?;

        let mcu = max_compute_units(device).unwrap_or(0);
        let mcf = max_clock_frequency(device).unwrap_or(0);
        println!(
            "Using device: {}, {}. Compute units: {}, Clock Freq: {}",
            device.name()?,
            device.vendor()?,
            mcu,
            mcf
        );

        let queue = Queue::new(&context, device, None)?;
        Ok(ContextQueue { context, queue })
    }

    pub fn system_default_device(p: Platform) -> Result<Device, DeviceLookupError> {
        Self::device_sort(Device::list(
            p,
            Some(ocl::flags::DeviceType::new().system_default()),
        )?)
        .ok_or(DeviceLookupError::NoSystemDefaultDeviceFound)
    }

    pub fn best_device(p: Platform) -> Result<Device, DeviceLookupError> {
        Self::device_sort(Device::list(
            p,
            Some(ocl::flags::DeviceType::new().gpu().accelerator()),
        )?)
        .ok_or(DeviceLookupError::NoGpusOrAcceleratorsFound)
    }

    fn device_sort(list: Vec<Device>) -> Option<Device> {
        list.into_iter().max_by_key(|d| {
            max_compute_units(*d).unwrap_or(0) * max_clock_frequency(*d).unwrap_or(0)
        })
    }
}
