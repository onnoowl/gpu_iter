extern crate proc_macro;

use crate::proc_macro::TokenStream;
use proc_macro2::{Ident, Span};
use quote::quote;
use syn::{self, Data, Fields, Type};

#[proc_macro_derive(ToGpu)]
pub fn to_gpu_derive(input: TokenStream) -> TokenStream {
    // Construct a representation of Rust code as a syntax tree
    // that we can manipulate
    let ast = syn::parse(input).unwrap();

    // Build the trait implementation
    impl_to_gpu(&ast)
}

#[allow(clippy::cognitive_complexity)]
fn impl_to_gpu(ast: &syn::DeriveInput) -> TokenStream {
    // These will be used for imports
    let gpu_iter_name =
        proc_macro_crate::crate_name("gpu_iter").expect("gpu_iter is present in `Cargo.toml`");
    let gpu_iter = Ident::new(&gpu_iter_name, Span::call_site());
    let ocl_name = proc_macro_crate::crate_name("ocl").expect("ocl is present in `Cargo.toml`");
    let ocl = Ident::new(&ocl_name, Span::call_site());

    let name = &ast.ident;
    let graph_expr_name = Ident::new(&format!("GpuType{}", name), Span::call_site());
    let prop_trait = Ident::new(&format!("{}GpuPropAccess", name), Span::call_site());

    let vis = ast.vis.clone();
    let (idents, types): (Vec<Ident>, Vec<Type>) = match &ast.data {
        Data::Struct(data_struct) => match &data_struct.fields {
            Fields::Named(fields_named) => fields_named
                .named
                .iter()
                .map(|field| {
                    (
                        field.ident.clone().expect("Expected field to be named"),
                        field.ty.clone(),
                    )
                })
                .unzip(),
            Fields::Unnamed(field_unnamed) => field_unnamed
                .unnamed
                .iter()
                .enumerate()
                .map(|(i, field)| {
                    (
                        Ident::new(&format!("s{}", i), Span::call_site()),
                        field.ty.clone(),
                    )
                })
                .unzip(),
            Fields::Unit => panic!("Unit structs are not supported for ToGpu derive"),
        },
        Data::Enum(_) => panic!("Enums are not supported for ToGpu derive"),
        Data::Union(_) => panic!("Unions are not supported for ToGpu derive"),
    };
    // We'll need a corresponding number of Generic paramter names we can use
    let generics: Vec<_> = idents
        .iter()
        .enumerate()
        .map(|(i, _)| Ident::new(&format!("G{}", i), Span::call_site()))
        .collect();

    // TODO: Verify repr("C"). See cbindgen for example

    let gen = quote! {
        impl #name {
            #vis fn from_expr<#(#generics: #gpu_iter::graph::IntoExpr<Type=#types>),*>(#(#idents: #generics),*) -> #gpu_iter::Expr<Self> {
                #gpu_iter::Expr::new(#graph_expr_name::__from_expr(#(#idents),*))
            }
        }

        impl #gpu_iter::get_type_str::RenderTypeStr for #name {
            fn impl_render(f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                write!(f, stringify!(#name))
            }
            fn needs_struct_defn() -> bool {
                true
            }
            fn impl_render_struct_defn(f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                <Self as #gpu_iter::to_gpu::HasGPUType>::impl_render_struct_defn(f)
            }
        }

        impl #gpu_iter::to_gpu::HasGPUType for #name {
            type GPUType = #graph_expr_name;

            fn impl_render_struct_defn(f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                use #gpu_iter::get_type_str::RenderTypeStr;

                writeln!(f, "typedef struct tag_{}\n{{", stringify!(#name))?;
                #(
                    writeln!(f, "\t{}\t{};", #types::render_type(), stringify!(#idents))?;
                )*
                writeln!(f, "}} {};", stringify!(#name))
            }
        }

        impl #gpu_iter::to_gpu::ToGpu for #name {
            fn add_to_kernel_builder<'s: 'b, 'k, 'b>(
                &'s self,
                kb: &'k mut #ocl::builders::KernelBuilder<'b>,
            ) -> &'k mut #ocl::builders::KernelBuilder<'b> {
                kb.arg(self)
            }
        }

        unsafe impl #ocl::traits::OclPrm for #name {

        }

        #[derive(Debug)]
        #vis struct #graph_expr_name {
            #(#idents: #gpu_iter::graph::DynNode),*
        }

        impl #graph_expr_name {
            #vis fn __from_expr<#(#generics: #gpu_iter::graph::IntoExpr<Type=#types>),*>(#(#idents: #generics),*) -> Self {
                #graph_expr_name { #(#idents: #idents.into_expr().node()),* }
            }
        }

        #vis trait #prop_trait {
            #(
                fn #idents(&self) -> #gpu_iter::Expr<#types>;
            )*
        }

        impl #prop_trait for #gpu_iter::Expr<#name> {
            #(
                #vis fn #idents(&self) -> #gpu_iter::Expr<#types> {
                    #gpu_iter::Expr::new(#gpu_iter::graph::PropertyAccess::new(self.node(), stringify!(#idents)))
                }
            )*
        }

        impl #gpu_iter::graph::ResolvesToType for #graph_expr_name {
            type Type = #name;
        }

        impl #gpu_iter::graph::Node for #graph_expr_name {
            fn call_on_children(&self, fun: &mut dyn FnMut(&[#gpu_iter::graph::DynNode])) {
                fun(&[#(self.#idents.clone()),*]);
            }

            fn render_with(
                &self,
                f: &mut std::fmt::Formatter<'_>,
                _self_key: u64,
                meta_map: &std::collections::HashMap<u64, #gpu_iter::graph::NodeMetaData>,
            ) -> std::fmt::Result {
                use #gpu_iter::get_type_str::RenderTypeStr;

                write!(
                    f,
                    "({}) {{",
                    Self::render_type(),
                )?;

                let renderers = [#(self.#idents.clone().renderer(meta_map)),*];
                for (i, r) in renderers.iter().enumerate() {
                    write!(f, "{}", r)?;
                    if i != renderers.len() - 1 {
                        write!(f, ", ")?;
                    }
                }
                write!(f, "}}")
            }
        }

    };
    gen.into()
}
