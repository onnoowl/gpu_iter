Old schedule:

- 1 week for finishing cache implementation
- 3 weeks for arbitrary typed inputs/outputs
------- We are here -------
- 3 weeks for either
	- Spir-v generation
	- gfx support (which would allow running in the browser (someday))
- 1 week for documenting work, creating guides, creating the final progress report

New Schedule:
- 1.5 week for finishing cache implementation
- 2.5 weeks for arbitrary typed inputs/outputs
------- We are here -------
- 1.5 weeks for finishing arbitrary typed inputs/outputs
- 1.5 weeks for fleshing out API Reduction methods, and other conveniences
- 1 week for documenting work, creating guides, creating the final progress report
